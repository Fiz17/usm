<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class Exports implements WithMultipleSheets
{
    public function sheets(): array
    {
       return [
            new Sheets\SummarySheet(),
            new Sheets\PendingOrdersSheet(),
            new Sheets\DeliveredOrdersSheet(),
            new Sheets\RentingRentalsSheet(),
            new Sheets\ReturnedRentalsSheet(),
            new Sheets\GroupProductsSheet(),
            new Sheets\GroupRentalsSheet(),
       ];
    }
}
