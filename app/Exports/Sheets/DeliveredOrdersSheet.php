<?php
namespace App\Exports\Sheets;

use App\Models\Order;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Facades\DB;

class DeliveredOrdersSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithStyles
{
    public function title(): string
    {
        return 'Delivered Orders'; // Set the name for this sheet
    }

    public function collection()
    {
        return Order::join('users', 'orders.matric_id', '=', 'users.matric_id')
                      ->join('products', 'orders.product_id', '=', 'products.product_id')
                      ->select(
                          DB::raw('DATE(orders.updated_at) as `Pickup Date`'),
                          'orders.order_id as Order ID',
                          'users.matric_id as Matric ID',
                          'users.name as Student Name',
                          'users.group_num as Group No.',
                          'users.email as Student Email',
                          'products.product_name as Product',
                          'orders.order_quantity as Quantity',
                          'orders.total_order_payment as Total Payment',
                      )
                      ->where('order_status', '=', 'delivered')
                      ->orderBy('Pickup Date', 'desc')
                      ->orderBy('users.group_num', 'desc')
                      ->get();
    }

    public function headings(): array
    {
        return ["Pickup Date", "Order ID", "Student Matric ID", "Student Name", "Student Group No.", "Student Email", "Product", "Quantity", "Total Payment"];
    }

    public function styles($sheet)
    {
        // Define your cell styles here
        return [
            1 => ['font' => ['bold' => true]], //Bold the header
            'E' => ['font' => ['bold' => true],], //Bold the Group No. column
        ];
    }
}