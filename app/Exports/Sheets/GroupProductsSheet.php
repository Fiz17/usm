<?php
namespace App\Exports\Sheets;

use App\Models\Rental;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Facades\DB;


class GroupProductsSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithStyles
{
    public function title(): string
    {
        return 'Groups Check - Product'; // Set the name for this sheet
    }

    public function collection()
    {
      return DB::table('users')
                ->leftJoin('orders', function ($join) {
                    $join->on('users.matric_id', '=', 'orders.matric_id');
                })
                ->select(
                    'users.group_num as Student Group No.',
                    DB::raw('CASE WHEN orders.matric_id IS NOT NULL THEN "Yes" ELSE NULL END AS `Ordered?`')
                )
                ->distinct()
                ->orderBy('users.group_num', 'desc')
                ->get();
    }

    public function headings(): array
    {
        return ["Student Group No.", "Ordered?"];
    }

    public function styles($sheet)
    {
        // Define your cell styles here
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}