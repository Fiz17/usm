<?php
namespace App\Exports\Sheets;

use App\Models\Rental;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Facades\DB;


class GroupRentalsSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithStyles
{
    public function title(): string
    {
        return 'Groups Check - Rental'; // Set the name for this sheet
    }

    public function collection()
    {
        return rental::join('users', 'rentals.matric_id', '=', 'users.matric_id')
                      ->join('bicycles', 'rentals.bicycle_id', '=', 'bicycles.bicycle_id')
                      ->select(
                          'users.group_num as Student Group No.',
                          DB::raw('SUM(rentals.total_payment) as `Total Payment`'),
                      )
                      ->where('rentals.status', '!=', 'pending')
                      ->groupBy('users.group_num')
                      ->orderBy('users.group_num', 'desc')
                      ->get();
    }

    public function headings(): array
    {
        return ["Student Group No.", "Total Payment Received (RM)"];
    }

    public function styles($sheet)
    {
        // Define your cell styles here
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }
}