<?php
namespace App\Exports\Sheets;

use App\Models\rental;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Facades\DB;

class RentingRentalsSheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithStyles
{
    public function title(): string
    {
        return 'Renting Bicycles'; // Set the name for this sheet
    }

    public function collection()
    {
        return rental::join('users', 'rentals.matric_id', '=', 'users.matric_id')
                      ->join('bicycles', 'rentals.bicycle_id', '=', 'bicycles.bicycle_id')
                      ->select(
                          DB::raw('DATE(rentals.rent_start_date) as `Start Date`'),
                          DB::raw('DATE(rentals.rent_end_date) as `End Date`'),
                          'bicycles.bicycle_id as Bicycle ID',
                          'rentals.rent_name as Rental Name',
                          'rentals.rent_phone_num as Rental Phone No.',
                          'users.name as Student Name',
                          'users.matric_id as Student Matric ID',
                          'users.group_num as Student Group No.',
                          'users.email as Student Email',
                          'rentals.total_payment as Total Payment',
                      )
                      ->where('rentals.status', '=', 'renting')
                      ->orderBy('End Date', 'asc')
                      ->orderBy('users.group_num', 'desc')
                      ->get();
    }

    public function headings(): array
    {
        return ["Start Date", "End Date", "Bicycle ID", "Rental Name", "Rental Phone No.", "Student Matric ID","Student Name","Student Group No.", "Student Email", "Total Payment"];
    }

    public function styles($sheet)
    {
        // Define your cell styles here
        return [
            1 => ['font' => ['bold' => true]],
            'H' => ['font' => ['bold' => true],],
        ];
    }
}