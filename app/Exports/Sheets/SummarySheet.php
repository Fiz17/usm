<?php
namespace App\Exports\Sheets;

use App\Models\Order;
use App\Models\Rental;
use App\Models\Product;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class SummarySheet implements FromCollection, WithTitle, WithHeadings, ShouldAutoSize, WithStyles
{
    public function title(): string
    {
        return 'Summary'; 
    }

    public function collection()
    {
        $summaryData = [];

        // Initialize a variable to keep track of the total payment
        $totalPayment = 0;

        // Get a list of all product names
        $productNames = Product::pluck('product_name')->toArray();

        foreach ($productNames as $productName) {
            $totalOrderPayment = Order::join('products', 'orders.product_id', '=', 'products.product_id')
                ->select(
                    'products.product_name as Product',
                    DB::raw('SUM(orders.total_order_payment) as `Total Payment`'),
                    'products.product_sold as Sold',
                    'products.product_stock as In Stock'
                )
                ->where('products.product_name', $productName)
                ->where('orders.order_status', '!=', 'pending')
                ->groupBy('products.product_name', 'products.product_sold', 'products.product_stock')
                ->first();

            if (!$totalOrderPayment) {
                $totalOrderPayment = [
                    'Product' => $productName, 
                    'Total Payment' => 0,
                    'Sold' => 0,
                    'In Stock' => Product::where('product_name', $productName)->value('product_stock'),
                ];
            }
    

            $summaryData[] = $totalOrderPayment;

            // Update the total payment
            $totalPayment += (float)$totalOrderPayment['Total Payment'];
        }

            $totalRentalPayment = Rental::select(DB::raw('SUM(rentals.total_payment) as `Total Payment`'))
                ->where('rentals.status', '!=', 'pending')
                ->first();

            // If no rental payment is found, set it to 0
            if (!$totalRentalPayment) {
                $totalRentalPayment = ['Product' => $productName, 'Total Payment' => 0];
            }

            $summaryData[] = ['Product' => 'Bicycle Rental', 'Total Payment' => $totalRentalPayment['Total Payment'], 'In Stock' => '', 'Sold' => ''];

            // Update the total payment
            $totalPayment += (float)$totalRentalPayment['Total Payment'];

            // Add the total row
            $summaryData[] = ['Product' => 'Total', 'Total Payment' => number_format($totalPayment, 2)];


        return collect($summaryData);
    }

    public function headings(): array
    {
        return ["Product Name/Rental", "Total Payment Received (RM)", "Sold", "In Stock"];
    }

    public function styles($sheet)
    {
      // Italic the Product Names  
      $sheet->getStyle('A2:A' . ($sheet->getHighestRow()))->applyFromArray([
            'font' => ['italic' => true],
        ]);

        //Bold the Total row
        $totalRow = $this->findTotalRow($sheet);
        if ($totalRow !== null) {
            // Apply bold style to the found total row
            $sheet->getStyle($totalRow)->getFont()->setBold(true);
        }

        // Bold the header
        return [
            1 => ['font' => ['bold' => true]],
        ];
    }

    // Find the row with "Total" in the "Product Name/Rental" column
    private function findTotalRow($sheet)
    {
        foreach ($sheet->getRowIterator() as $row) {
            $cell = $sheet->getCell('A' . $row->getRowIndex());
            if ($cell->getValue() === 'Total') {
                return $row->getRowIndex();
            }
        }

        // Return null if the "Total" row is not found
        return null;
    }

    // public function headings(): array
    // {
    //     $fridays = $this->getFridays();
    //     $headings = ["Product Name/Rental"];

    //     foreach ($fridays as $friday) {
    //         $headings[] = $friday->format('Y-m-d');
    //     }

    //     return $headings;
    // }

    // private function getFridays()
    // {
    //     $fridays = [];
    //     $earliestPickupDate = Order::min('updated_at'); // Find the earliest pickup date in orders
    //     $earliestStartDate = Rental::min('rent_start_date'); // Find the earliest start date in rentals
    //     $startDate = min($earliestPickupDate, $earliestStartDate); // Use the earliest of the two

    //     if (!$startDate) {
    //         // If no start date found, use the current date as a fallback
    //         $startDate = Carbon::now()->setTimezone('Asia/Kuala_Lumpur');
    //     } else {
    //         $startDate = Carbon::parse($startDate)->setTimezone('Asia/Kuala_Lumpur');
    //     }

    //     // Calculate the latest date
    //     $latestPickupDate = Order::max('pickup_date');
    //     $latestStartDate = Rental::max('rent_start_date');
    //     $latestDate = max($latestPickupDate, $latestStartDate);

    //     if (!$latestDate) {
    //         // If no latest date found, use the current date as a fallback
    //         $latestDate = Carbon::now()->setTimezone('Asia/Kuala_Lumpur');
    //     } else {
    //         $latestDate = Carbon::parse($latestDate)->setTimezone('Asia/Kuala_Lumpur');
    //     }

    //     while ($startDate->dayOfWeek !== Carbon::FRIDAY) {
    //         $startDate->addDay();
    //     }

    //     while ($startDate->lte($latestDate)) {
    //         $fridays[] = $startDate->copy();
    //         $startDate->addWeek();
    //     }

    //     return $fridays;
    // }
}