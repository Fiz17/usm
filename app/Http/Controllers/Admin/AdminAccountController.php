<?php

namespace App\Http\Controllers\admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        /**
         * To display on the admin tables
         */
        $admins = Admin::select(['id','name','email','type','created_at'])->sortable(['created_at' => 'desc'])->paginate(10);

         /**
         * To return the view folder: resource/views/admin/adminaccount
         */
        return view('admin.page.adminaccount', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.auth.register');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
       return view('admin.adminaccount');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $admin = Admin::findorfail($id);
        /**
         * Find the Admin id -> return to edit page
         */
        return view('admin.edit.editadmin', ['admin' => $admin]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $admin = Admin::findorfail($id);

        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $admin->email = $request->email;
        $admin->type = $request->type;

        $admin->save();

        session()->flash('info','Successfully updated the admin account.');

        /**
         * Redirect to admin account page
         */
        return redirect()->route('adminaccount.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $admin = Admin::find($id);
        
        $admin->delete();

        session()->flash('delete','Admin account deleted succesfully.');
        /**
         * Redirect to admin account page
         */
        return redirect()->route('adminaccount.index');
    }
}
