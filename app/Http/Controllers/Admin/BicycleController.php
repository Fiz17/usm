<?php

namespace App\Http\Controllers\admin;

use App\Models\Bicycle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BicycleController extends Controller
{
    /**
     * Display a listing of the resource for Bicycle Controller.
     */
    public function index()
    {
        /**
         * To display on the bicycles table
         */
        $bicycles = Bicycle::select(['bicycle_id','bicycle_condition','created_at'])->sortable(['created_at' => 'desc'])->paginate(10);

        /**
         * This section is to count the number of bicycle conditions 
         * either "good" or "bad".
         */
        $goodCount = Bicycle::where('bicycle_condition', 'good')->count();
        $badCount = Bicycle::where('bicycle_condition', 'bad')->count();
        $allCount = Bicycle::count();
        
        /**
         * To return the view folder: resource/views/admin/bicycle
         */
        return view('admin.page.bicycle', compact('bicycles','goodCount', 'badCount', 'allCount',));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        /**
         * To return view folder: resource/views/admin/create/createbicycle
         */
        return view('admin.create.createbicycle', compact('bicycleId'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'bicycle_id'=> ['required','max:4'],
            'bicycle_desc' => ['required','max:100'],
            // 'BICYCLE_PRICE' => ['required','numeric','min:0'],
            // 'BICYCLE_PRICE_COMMISSION' => ['required','numeric','min:0'],
        ]);
        $bicycle = new Bicycle();
        
        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $bicycle->bicycle_id = $request->bicycle_id;
        $bicycle->bicycle_condition = $request->bicycle_condition;
        $bicycle->bicycle_desc = $request->bicycle_desc;
        $bicycle->bicycle_price = $request->bicycle_price;
        $bicycle->bicycle_price_commision = $request->bicycle_price_commision;
        
        $bicycle->save();

        session()->flash('success','Successfully added new bicycle.');
        /**
         * Redirect to bicycle page
         */
        return redirect()->route('bicycle.index');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        /**
         * return view to bicycle page because we are not using this function
         */
        return view('admin.bicycle');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //dd($id);
        /**
         * Find the Bicycle id -> return to edit page
         */
        $bicycle = Bicycle::findorfail($id);

        return view('admin.edit.editbicycle', compact('bicycle'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $bicycle = Bicycle::findorfail($id);

        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $bicycle->bicycle_id = $request->bicycle_id;
        $bicycle->bicycle_condition = $request->bicycle_condition;
        $bicycle->bicycle_desc = $request->bicycle_desc;
        $bicycle->bicycle_price = $request->bicycle_price;
        $bicycle->bicycle_price_commision = $request->bicycle_price_commision;

        $bicycle->save();

        session()->flash('info','Successfully updated the bicycle.');
        /**
         * Redirect to bicycle page
         */
        return redirect()->route('bicycle.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        /**
         * Find Bicycle -> delete -> back to product page
         */
        $bicycle = Bicycle::find($id);

        $bicycle->delete();

        session()->flash('delete','Bicycle deleted succesfully.');

        return redirect()->route('bicycle.index');
    }

    public function search(Request $request)
    {
        $bicycles = Bicycle::select(['bicycle_id','bicycle_condition','created_at'])->sortable(['created_at' => 'desc'])->paginate(10);

        $goodCount = Bicycle::where('bicycle_condition', 'good')->count();
        $badCount = Bicycle::where('bicycle_condition', 'bad')->count();
        $allCount = Bicycle::count();

        $query = $request->input('query');
        
        $searchResults = Bicycle::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('bicycle_id', 'LIKE', '%' . $query . '%')
                         ->orWhere('bicycle_condition', 'LIKE', '%' . $query . '%');
        })->paginate(10);

        if ($searchResults->isEmpty()) {
            return redirect()->route('bicycle.index')->with('info', 'No search results found.');
        }
        
        return view('admin.page.bicycle', compact('searchResults','bicycles','goodCount', 'badCount', 'allCount'));
    }
}