<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Order;
use App\Models\Rental;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orders = Order::with('product', 'user')->sortable(['created_at' => 'desc'])->paginate(5);
        $rentals = Rental::with('bicycle', 'user')->sortable(['created_at' => 'desc'])->paginate(5);

        $accountCreated = User::where('status', 'verified')->count();
        $pendingOrder = Order::where('order_status', 'pending')->count();
        $pendingRental = Rental::where('status', 'pending')->count();
        $accountCreate = User::where('status', 'verified')->count();

        return view('admin.page.dashboard', compact('orders','rentals','pendingOrder','pendingRental','accountCreate'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
