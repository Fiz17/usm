<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\Order;
use App\Models\Product;

use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $orders = Order::with('product', 'user')->sortable(['created_at' => 'desc'])->paginate(10);

        $pendingCount = Order::where('order_status', 'pending')->count();
        $finishCount = Order::where('order_status', 'delivered')->count();

        return view('admin.page.order', compact('orders','pendingCount','finishCount'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $order = Order::findorfail($id);

        return view('admin.edit.editorder', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $order = Order::findorfail($id);

        $order->order_id = $request->order_id;
        $order->product_id = $request->product_id;
        $order->matric_id = $request->matric_id;
        $order->order_quantity = $request->order_quantity;
        $order->pickup_date = $request->pickup_date;
        $order->total_order_payment = $request->total_order_payment;
        $order->order_status = $request->order_status;

        $order->save();
        
        // Update product_stock and product_sold if order_status is 'finish'
        if ($order->order_status === 'delivered') {
            $product = Product::findOrFail($order->product_id);
            
            $product->product_stock -= $order->order_quantity;
            $product->product_sold += $order->order_quantity;

            $product->save();
        }

        session()->flash('info','Successfully updated order status.');

        return redirect()->route('order.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $order = Order::find($id);

        $order->delete();

        session()->flash('delete','Order deleted succesfully.');

        return redirect()->route('order.index');
    }

    /**
     * Generate Order Receipt in PDF
     */
    public function print($id)
    {
        $order = Order::findorfail($id);
        $currentDate = date('d/m/Y');
        $adminName = auth()->user()->name;

        $dompdf = new Dompdf();
        $html = View::make('admin.edit.orderreceipt', compact('order', 'currentDate', 'adminName'));
        
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        
        $filename = $order->order_id . '_Receipt.pdf';

        return response($dompdf->output())
        ->header('Content-Type', 'application/pdf')
        ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
    }

    public function search(Request $request)
    {
        $orders = Order::with('product', 'user')->sortable(['created_at' => 'desc'])->paginate(10);

        $pendingCount = Order::where('order_status', 'pending')->count();
        $finishCount = Order::where('order_status', 'delivered')->count();

        $query = $request->input('query');
        
        $searchResults = Order::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('order_status', 'LIKE', '%' . $query . '%')
                         ->orWhereHas('product', function ($subQuery) use ($query) {
                             $subQuery->where('product_name', 'LIKE', '%' . $query . '%');
                         })
                         ->orWhereHas('user', function ($subQuery) use ($query) {
                             $subQuery->where('group_num', 'LIKE', '%' . $query . '%');
                         });
        })->paginate(10);

        if ($searchResults->isEmpty()) {
            return redirect()->route('order.index')->with('info', 'No search results found.');
        }
        
        return view('admin.page.order', compact('searchResults','orders','pendingCount','finishCount'));
    }
}
