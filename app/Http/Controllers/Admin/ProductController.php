<?php

namespace App\Http\Controllers\admin;

use App\Models\Stock;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        /**
         * To display on the products table
         */
        $products = Product::select(['product_id','product_name','product_stock','created_at'])->sortable(['created_at' => 'desc'])->paginate(10);

        /**
         * This section is to count the number of total product stock
         * either "totalStock" or "totaliniStock" also count totalSold.
         */
        $totalStock = Product::sum('product_stock');
        $totaliniStock = Product::sum('product_ini_stock');
        $totalSold = $totaliniStock - $totalStock;

        /**
         * To return the view folder: resource/views/admin/product
         */
        return view('admin.page.product', compact('products','totalStock','totaliniStock','totalSold'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        /**
         * To return view folder: resource/views/admin/create/createproduct
         */
        return view('admin.create.createproduct');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $product = new Product();
        
        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $product->product_id = $request->product_id;
        $product->product_name = $request->product_name;
        $product->product_desc = $request->product_desc;
        $product->product_price = $request->product_price;
        $product->product_price_com = $request->product_price_com;
        $product->product_ini_stock = $request->product_ini_stock;
        $product->product_stock = $request->product_stock;

        $product->save();

        session()->flash('success','Successfully added new product.');
        /**
         * Redirect to product page
         */
        return redirect()->route('product.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::findorfail($id);
        
        $stocks = Stock::where('product_id', $id)->with('product')->sortable(['created_at' => 'desc'])->paginate(10);

        $product_ini_stock = $product->product_ini_stock;
        $product_stock = $product->product_stock;

        $totalStock = Stock::where('product_id', $id)->sum('stock_quantity');
        $totalSold = $product_ini_stock - $totalStock;

        return view('admin.page.stock', compact('stocks', 'product', 'totalStock', 'product_ini_stock', 'product_stock', 'totalSold'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        /**
         * Find the Product id -> return to edit page
         */
        $product = Product::findorfail($id);

        return view('admin.edit.editproduct', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $product = Product::findorfail($id);

        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $product->product_id = $request->product_id;
        $product->product_name = $request->product_name;
        $product->product_desc = $request->product_desc;
        $product->product_price = $request->product_price;
        $product->product_price_com = $request->product_price_com;
        $product->product_ini_stock = $request->product_ini_stock;
        $product->product_stock = $request->product_stock;

        $product->save();

        session()->flash('info','Successfully updated the product.');
        /**
         * Redirect to product page
         */
        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        /**
         * Find Product -> delete -> back to product page
         */
        $product = Product::find($id);

        $product->delete();

        session()->flash('delete','Product deleted succesfully.');

        return redirect()->route('product.index');
    }

    public function search(Request $request)
    {
        $products = Product::select(['product_id','product_name','product_stock','created_at'])->sortable(['created_at' => 'desc'])->paginate(10);

        $totalStock = Product::sum('product_stock');
        $totaliniStock = Product::sum('product_ini_stock');
        $totalSold = $totaliniStock - $totalStock;

        $query = $request->input('query');
        
        $searchResults = Product::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('product_id', 'LIKE', '%' . $query . '%')
                         ->orWhere('product_name', 'LIKE', '%' . $query . '%');
        })->paginate(10);

        if ($searchResults->isEmpty()) {
            return redirect()->route('product.index')->with('info', 'No search results found.');
        }
        
        return view('admin.page.product', compact('searchResults','products','totalStock','totaliniStock','totalSold'));
    }
}
