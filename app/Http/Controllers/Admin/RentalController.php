<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Rental;

use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;

class RentalController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rentals = Rental::with('bicycle', 'user')->sortable(['created_at' => 'desc'])->paginate(10);
        
        $pendingCount = Rental::where('status', 'pending')->count();
        $processCount = Rental::where('status', 'renting')->count();
        $finishCount = Rental::where('status', 'returned')->count();

        return view('admin.page.rental',compact('rentals','pendingCount','processCount','finishCount'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $rental = Rental::findorfail($id);

        return view('admin.edit.editrental', compact('rental'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $rental = Rental::findorfail($id);

        $rental->rental_id = $request->rental_id;
        $rental->bicycle_id = $request->bicycle_id;
        $rental->rent_name = $request->rent_name;
        $rental->rent_matric_id = $request->rent_matric_id;
        $rental->rent_phone_num = $request->rent_phone_num;
        $rental->rent_start_date = $request->rent_start_date;
        $rental->rent_end_date = $request->rent_end_date;
        $rental->total_payment = $request->total_payment;
        $rental->status = $request->status;

        $rental->save();

        session()->flash('info','Rental status updated successfully.');

        return redirect()->route('rental.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $rental = Rental::find($id);

        $rental->delete();

        session()->flash('delete','Rental deleted succesfully.');

        return redirect()->route('rental.index');
    }

    public function print($id)
    {
        $rental = Rental::findorfail($id);
        $currentDate = date('d/m/Y');
        $adminName = auth()->user()->name;

        if($rental->status == 'pending' || $rental->status === 'renting'){
            $viewName = 'admin.edit.rentalreceipt';
        }elseif($rental->status == 'returned'){
            $viewName = 'admin.edit.rentalreturnedreceipt';
        }
        
        $dompdf = new Dompdf();
        $html = View::make($viewName, compact('rental', 'currentDate', 'adminName'));
        
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $filename = $rental->rental_id . '_Receipt.pdf';

        return response($dompdf->output())
        ->header('Content-Type', 'application/pdf')
        ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
    }

    public function search(Request $request)
    {
        $rentals = Rental::with('bicycle', 'user')->sortable(['created_at' => 'desc'])->paginate(10);
        
        $pendingCount = Rental::where('status', 'pending')->count();
        $processCount = Rental::where('status', 'renting')->count();
        $finishCount = Rental::where('status', 'returned')->count();

        $query = $request->input('query');
        
        $searchResults = Rental::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('status', 'LIKE', '%' . $query . '%')
                        ->orWhere('bicycle_id', 'LIKE', '%' . $query . '%')
                         ->orWhereHas('user', function ($subQuery) use ($query) {
                             $subQuery->where('group_num', 'LIKE', '%' . $query . '%');
                         });
        })->paginate(10);

        if ($searchResults->isEmpty()) {
            return redirect()->route('rental.index')->with('info', 'No search results found.');
        }
        
        return view('admin.page.rental', compact('searchResults','rentals','pendingCount','processCount','finishCount'));
    }
}
