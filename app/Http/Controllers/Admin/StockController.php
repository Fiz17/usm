<?php

namespace App\Http\Controllers\admin;

use App\Models\Stock;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // $stock_id = '';

        // do {
        //     $stock_id = 'STO-' . rand(1000, 9999);
        // } while (Stock::where('stock_id', $stock_id)->exists());

        // return view('admin.create.createstock', compact('stock_id'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $stock = new Stock();
        
        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $stock->stock_id = $request->stock_id;
        $stock->product_id = $request->product_id;
        $stock->stock_date = $request->stock_date;
        $stock->stock_quantity = $request->stock_quantity;

        $stock->save();

        // Update the product's stock values
        $product = Product::find($request->product_id);
        $product->product_stock += $request->stock_quantity;
        $product->product_ini_stock += $request->stock_quantity;
        $product->save();

        session()->flash('success','Successfully added new stock.');
        /**
         * Redirect to product page
         */
        return redirect()->route('product.show' ,['product' => $request->product_id]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $product = Product::findorfail($id);

        $stock_id = '';

        do {
            $stock_id = 'STO-' . rand(1000, 9999);
        } while (Stock::where('stock_id', $stock_id)->exists());

        //dd($product);
        return view('admin.create.createstock', compact('stock_id', 'product'));
    }
    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $stock = Stock::findorfail($id);

        return view('admin.edit.editstock', compact('stock'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $stock = Stock::findorfail($id);
        
        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $stock->stock_id = $request->stock_id;
        $stock->product_id = $request->product_id;
        $stock->stock_date = $request->stock_date;
        $stock->stock_quantity = $request->stock_quantity;

        $stock->save();

        // Update the product's stock values
        $product = Product::find($request->product_id);
        $product->product_stock += $request->stock_quantity;
        $product->product_ini_stock += $request->stock_quantity;
        $product->save();

        session()->flash('info','Successfully updated the stock.');
        /**
         * Redirect to product page
         */
        return redirect()->route('product.show');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        /**
         * Find Product -> delete -> back to product page
         */
        $stock = Stock::find($id);
        
        if ($stock) {
           $product = Product::find($stock->product_id);
    
            // Update the product's stock values
            $product->product_stock -= $stock->stock_quantity;
            $product->product_ini_stock -= $stock->stock_quantity;
            $product->save();
    
            $stock->delete();
    
            session()->flash('delete', 'Stock deleted successfully.');
        } else {
            session()->flash('error', 'Stock not found.');
        }

        return redirect()->route('product.show');
    }
}
