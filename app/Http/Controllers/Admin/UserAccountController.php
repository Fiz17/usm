<?php

namespace App\Http\Controllers\admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::select(['group_num','matric_id','name','email','phone_num','status','updated_at'])->sortable(['updated_at' => 'desc'])->paginate(10);
        

        $verified = User::where('status', 'verified')->count();
        $unverified = User::where('status', 'unverified')->count();
        $total = User::count();


        return view('admin.page.useraccount' ,compact('users','verified','unverified','total'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.create.uploadaccount');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::find($id);
        /**
         * Find the Admin id -> return to edit page
         */
        return view('admin.edit.edituser', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user = User::findorfail($id);

        /**
         * TO GET THE DATA.
         * Left from database , Right from form (the name)
         */
        $user->matric_id = $request->matric_id;
        $user->name = $request->name;
        $user->group_num = $request->group_num;
        $user->phone_num = $request->phone_num;
        $user->email = $request->email;
        $user->status = $request->status;

        $user->save();

        /**
         * Redirect to admin account page
         */
        return redirect()->route('useraccount.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::find($id);
        
        $user->delete();

        /**
         * Redirect to admin account page
         */
        return redirect()->route('useraccount.index');
    }

    public function search(Request $request)
    {
        $users = User::select(['group_num','matric_id','name','email','phone_num','status','updated_at'])->sortable(['updated_at' => 'desc'])->paginate(10);
        

        $verified = User::where('status', 'verified')->count();
        $unverified = User::where('status', 'unverified')->count();
        $total = User::count();

        $query = $request->input('query');
        
        $searchResults = User::where(function ($queryBuilder) use ($query) {
            $queryBuilder->where('group_num', 'LIKE', '%' . $query . '%')
                         ->orWhere('matric_id', 'LIKE', '%' . $query . '%')
                         ->orWhere('name', 'LIKE', '%' . $query . '%')
                         ->orWhere(function ($subquery) use ($query) {
                            $subquery->where('status', '=', $query);
                        });
        })->paginate(10);

        if ($searchResults->isEmpty()) {
            return redirect()->route('useraccount.index')->with('info', 'No search results found.');
        }
        
        return view('admin.page.useraccount' ,compact('searchResults', 'users','verified','unverified','total'));
    }
}
