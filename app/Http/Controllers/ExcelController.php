<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use App\Exports\Exports;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class ExcelController extends Controller
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function import()
    {
        try{
            Excel::import(new UsersImport,request()->file('file'));

            // Flash a success message to the session
            Session::flash('success', 'Students Data Uploaded Successfully.');

            //Redirect back to the previous page with the success message
            return back();

        }catch (\Exception $e) {
             Log::error("Error uploading excel file: {$e->getMessage()}");
             Session::flash('error', 'An error occurred while uploading students data.');

             return back();
         }
        
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        // try{
            $currentDate = date('Ymd');
            $filename = 'WUS101_Weekly_Report_' . $currentDate . '.xlsx';

            return Excel::download(new Exports, $filename);

        // }catch (\Exception $e) {
        //     Log::error("Error downloading excel file: {$e->getMessage()}");

        //     return redirect()->route('admin.dashboard')->with('error', 'An error occurred while downloading weekly report.');
        // }
    }
}
