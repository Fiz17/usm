<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Order;
use App\Models\Rental;
use App\Mail\SampleMail;
use App\Mail\ReminderOrderRental;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ReminderController extends Controller
{
    public function registerReminder()
    {
        try{
            // Fetch recipient emails and names from the database
            $recipients = User::select('group_num', 'email')->where('status', 'unverified')->get(); 
            $emailSent = true;

            // Send email to each recipient
            foreach ($recipients as $recipient) {
                $emailData = [
                    'subject' => 'WUS101 SYSTEM REMINDER',
                    'message' => 
                    "Dear Group {$recipient->group_num} students, Please register in the WUS101 System ASAP to ease the process of ordering products. Thank you.",
                ];

                if(!Mail::to($recipient->email)->send(new SampleMail($emailData))) {
                    $emailSent = false;
                    break;
                }  
            }

            if($emailSent){
                // Flash a success message to the session
                Session::flash('success', 'Registration Reminders Sent Successfully.');
            } else {
                // Flash an error message
                Session::flash('error', 'Registration Reminder Sending Fails.');
            }
        } catch (\Exception $e) {
            Log::error("Error sending registration reminders: {$e->getMessage()}");
            Session::flash('error', 'An error occurred while sending registration reminders.');
        }

        return back();
    }

    public function pendingorderReminder()
    {
        try{
            $currentDate = Carbon::now();

            // Fetch recipient emails and names from the database for users with pending orders
            $recipients = Order::with(['user' => function ($query) {
                $query->select('matric_id', 'group_num', 'email');
            }, 'product' => function ($query){
                $query->select('product_id', 'product_name');
            }])
            ->where('order_status', 'pending')
            ->where('pickup_date', '<', $currentDate)
            ->get();

            $emailSent = true;

            // Send email to each recipient
            foreach ($recipients as $recipient) {
                $emailData = [
                    'subject' => 'WUS101 SYSTEM REMINDER',
                    'message' => 
                    "Dear Group {$recipient->user->group_num} students, you have pending order. Please collect your {$recipient->product->product_name} products as soon as possible at Pusat Rancangan Kokurikulum. Thank you.",
                ];

                if (!Mail::to($recipient->user->email)->send(new ReminderOrderRental($emailData))) {
                    $emailSent = false;
                    break;
                }
            }

            if ($emailSent) {
                // Flash a success message to the session
                Session::flash('success', 'Pending Order Reminders Sent Successfully.');
            } else {
                Session::flash('error', 'Pending Order Reminder Sending Fails.');
            }
        } catch (\Exception $e) {
            Log::error("Error sending pending order reminders: {$e->getMessage()}");
            Session::flash('error', 'An error occurred while sending pending order reminders.');
        }

        return back();

    }

    public function returnbicycleReminder()
    {
        try{
            $currentDate = Carbon::now();

            // Fetch recipient emails and names from the database for users with pending orders
            $recipients = Rental::with(['user' => function ($query) {
                    $query->select('matric_id', 'group_num', 'email');},
            ])
            ->select('matric_id', 'bicycle_id', 'rent_name', 'rent_end_date')
            ->where('status', '!=', 'returned')
            ->where('rent_end_date', '<', $currentDate)
            ->get();

            $emailSent = true;

            // Send email to each recipient
            foreach ($recipients as $recipient) {
                $emailData = [
                    'subject' => 'WUS101 SYSTEM REMINDER',
                    'message' => 
                    "Dear Group {$recipient->user->group_num} students, the {$recipient->bicycle_id} bicycle you rented out to your customer, {$recipient->rent_name} was supposed to be returned by {$recipient->rent_end_date}. Please remind your customer to return it a ASAP. Thank you.",
                ];

                if (!Mail::to($recipient->user->email)->send(new ReminderOrderRental($emailData))) {
                    $emailSent = false;
                    break;
                }
            }

            if ($emailSent) {
                // Flash a success message to the session
                Session::flash('success', 'Return Bicycle Reminders Sent Successfully.');
            } else {
                Session::flash('error', 'Return Bicycle Reminder Sending Fails.');
            }
        }catch (\Exception $e) {
             Log::error("Error sending return bicycle reminders: {$e->getMessage()}");
             Session::flash('error', 'An error occurred while sending return bicycle reminders.');
        }

        return back();

    }
}
