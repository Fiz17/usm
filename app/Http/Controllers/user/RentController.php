<?php

namespace App\Http\Controllers\user;

use App\Models\Rental;
use App\Models\Bicycle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $rentals = Rental::all();

        // Get the IDs of already rented bicycles
        $rentedBicycleIds = Rental::pluck('bicycle_id')->toArray();

        // Retrieve available bicycles that haven't been rented yet
        $availableBicycles = Bicycle::where('bicycle_condition', 'good')->whereNotIn('bicycle_id', $rentedBicycleIds)->get();
        // Retrieve bicycle IDs with "finish" status from Rental model
        $finishBicycleIds = Rental::where('status', 'finish')->pluck('bicycle_id')->toArray();

        // Retrieve bicycles with "finish" status from Bicycle model
        $finishBicycles = Bicycle::whereIn('bicycle_id', $finishBicycleIds)->get();

        $rental_id = '';
        
        do {
            $rental_id = 'R-' . rand(1000, 9999);
        } while (Rental::where('rental_id', $rental_id)->exists());

        return view('user.page.rent',compact('rentals','availableBicycles','finishBicycles','rental_id'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('user.rent');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $rental = new Rental();

        $rental->rental_id = $request->rental_id;
        $rental->bicycle_id = $request->bicycle_id;
        $rental->rent_name = $request->rent_name;
        $rental->matric_id = Auth::user()->matric_id;
        $rental->rent_matric_id = $request->rent_matric_id;
        $rental->rent_phone_num = $request->rent_phone_num;
        $rental->rent_start_date = $request->rent_start_date;
        $rental->rent_end_date = $request->rent_end_date;
        $rental->total_payment = $request->total_payment;

        $rental->save();

        session()->flash('success','You have successfully submit rental order.');

        return redirect()->route('rent.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return view('user.rent');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        // $rental = Rental::findorfail($id);

        // return view('admin.edit.editbicycle', compact('bicycle'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $rental = Rental::findorfail($id);

        $rental->rental_id = $request->rental_id;
        $rental->bicycle_id = $request->bicycle_id;
        $rental->rent_name = $request->rent_name;
        $rental->rent_matric_id = $request->rent_matric_id;
        $rental->rent_phone_num = $request->rent_phone_num;
        $rental->rent_start_date = $request->rent_start_date;
        $rental->rent_end_date = $request->rent_end_date;
        $rental->total_payment = $request->total_payment;
        $rental->status = $request->status;

        $rental->save();

        return redirect()->route('rent.index');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $rental = Rental::find($id);

        $rental->delete();

        //session()->flash('delete','Bicycle deleted succesfully.');

        return redirect()->route('rental.index');
    }
}
