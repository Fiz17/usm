<?php

namespace App\Http\Controllers\user;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $orders = Order::with('Product')->get();
        $products = Product::all();

        $order_id = '';
        
        do {
            $order_id = 'ORD-' . rand(1000, 9999);
        } while (Order::where('order_id', $order_id)->exists());


        return view('user.page.shop',compact('orders','products','order_id'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $order = new Order();

        $order->order_id = $request->order_id;
        $order->product_id = $request->product_id;
        $order->matric_id = Auth::user()->matric_id;
        $order->order_quantity = $request->order_quantity;
        $order->total_order_payment = $request->total_order_payment;
        $order->pickup_date = $request->pickup_date;

        $order->save();

        session()->flash('success','You have successfully submit the order.');

        return redirect()->route('shop.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
