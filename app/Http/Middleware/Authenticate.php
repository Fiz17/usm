<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
        // Check if the intended URL starts with '/admin'
        if (str_starts_with($request->path(), 'admin')) {
            return route('admin.login'); // Redirect to admin login
        }
        return route('login'); // Redirect to regular login
        
        // return $request->expectsJson() ? null : route('login');
    }
}
