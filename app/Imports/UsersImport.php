<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Hash;

class UsersImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new User([
            'matric_id' => $row['matric_id'],
            'name'      => $row['name'],
            'email'     => $row['email'], 
            'password'  => $row['password'],
            'phone_num' => $row['phone_num'],
            'group_num' => $row['group_num'],
        ]);
    }
}