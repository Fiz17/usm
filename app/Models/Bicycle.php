<?php

namespace App\Models;

use App\Models\Rental;
use Kyslik\ColumnSortable\Sortable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bicycle extends Model
{
    use HasFactory, Sortable;

    public $incrementing = false;
    protected $primaryKey = 'bicycle_id';
    
    public $sortable = ['bicycle_id', 'bicycle_condition', 'created_at'];

    public function rentals()
    {
        return $this->hasOne(Rental::class);
    }
}
