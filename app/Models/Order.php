<?php

namespace App\Models;

use App\Models\User;
use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Kyslik\ColumnSortable\Sortable;

class Order extends Model
{
    use HasFactory, Sortable;

    public $incrementing = false;
    protected $primaryKey = 'order_id';

    protected $fillable = [
        'order_id',
        'product_id',
        'order_quantity',
        'pickup_date',
        'total_order_payment',
        'status'
    ];

    public $sortable = ['order_id', 'order_quantity', 'pickup_date','total_order_payment','status', 'created_at'];

    public function user()
    {
        return $this->belongsTo(User::class, 'matric_id', 'matric_id');
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }
}
