<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Stock;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory, Sortable;
    
    public $incrementing = false;
    protected $primaryKey = 'product_id';

    public $sortable = ['product_id', 'product_name', 'product_stock', 'created_at'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }
}
