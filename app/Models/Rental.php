<?php

namespace App\Models;

use App\Models\User;
use App\Models\Bicycle;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Kyslik\ColumnSortable\Sortable;

class Rental extends Model
{
    use HasFactory,Sortable;

    protected $primaryKey = 'rental_id';
    public $incrementing = false; // To prevent auto-incrementing primary key

    protected $fillable = [
        'rental_id',
        'bicycle_id',
        'rent_name',
        'rent_matric_id',
        'rent_phone_num',
        'rent_start_date',
        'rent_end_date',
        'total_payment',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'matric_id', 'matric_id');
    }
    
    public function bicycle()
    {
        return $this->belongsTo(Bicycle::class);
    }
}
