<?php

namespace App\Models;

use App\Models\Product;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Stock extends Model
{
    use HasFactory,Sortable;

    public $incrementing = false;
    protected $primaryKey = 'stock_id';

    public $sortable = ['stock_id', 'stock_quantity', 'stock_date', 'created_at'];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }
}