<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Kyslik\ColumnSortable\Sortable;

class Testing extends Model
{
    use HasFactory, Sortable;

     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    //public $timestamps = false;

     protected $fillable = [
       'MATRIC_ID',
       'CUS_NAME',
       'CUS_PASSWORD',
       'CUS_PHONE',
       'CUS_EMAIL',
       'CUS_GROUP',
    ];

    public $sortable = [
      'MATRIC_ID',
       'CUS_NAME',
       'CUS_GROUP',
    ];
}
