<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Rental;
use Laravel\Sanctum\HasApiTokens;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable, Sortable;

    protected $guard = 'web';
    public $incrementing = false;
    protected $primaryKey = 'matric_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'matric_id',
        'name',
        'email',
        'password',
        'phone_num',
        'group_num',
        'status',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public $sortable = ['matric_id', 'name', 'email', 'password','phone_num','group_num', 'status','created_at'];

    public function rentals()
    {
        return $this->hasMany(Rental::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
