<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Admin;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Admin>
 */

class AdminFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        //$adminType = Admin::count() === 0 ? 'master' : 'normal';

        return [
            'ADMIN_EMAIL' => $this->faker->unique()->safeEmail,
            'ADMIN_PASSWORD' => bcrypt('secret'),
            //'ADMIN_TYPE' => $adminType,
            'ADMIN_TYPE' => $this->faker->randomElement(['master','normal']),
        ];
    }
}

