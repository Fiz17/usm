<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Bicycle>
 */
class BicycleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'bicycle_id' => $this->faker->unique()->numberBetween(1, 9999),
            'bicycle_condition'=> $this->faker->randomElement(['good','bad']),
            'bicycle_desc' => $this->faker->text(),
            'bicycle_price' => $this->faker->randomFloat(2,10,99),
            'bicycle_price_commision' => $this->faker->randomFloat(2,1,99)
        ];

        
    }
}
