<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    protected $model = Product::class;
    
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        // Generate random values for PRODUCT_STOCK and PRODUCT_INI_STOCK
        $productStock =  $this->faker->numberBetween(0, 50); // You can adjust the range as needed
        $productIniStock = $this->faker->numberBetween(51, 200); // You can adjust the range as needed

        // Calculate PRODUCT_SOLD as the difference between PRODUCT_INI_STOCK and PRODUCT_STOCK
        $productSold = max($productIniStock - $productStock, 0);

        return [
            'product_id' => $this->faker->unique()->numberBetween(1, 9999),
            'product_name' => $this->faker->name(),
            'product_desc' => $this->faker->text(),
            'product_price' => $this->faker->randomFloat(2, 1, 1000), // Random decimal with 2 decimal places
            'product_price_com' => $this->faker->randomFloat(2, 1, 1000), // Random decimal with 2 decimal places
            'product_stock' => $productStock,
            'product_ini_stock' => $productIniStock,
            'product_sold' => $productSold
        ];
    }
}
