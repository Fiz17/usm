<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('bicycles', function (Blueprint $table) {
            $table->string('bicycle_id')->primary();         // Primary key column
            $table->enum('bicycle_condition', ['good', 'bad']); // Enum column with allowed values 'good' or 'bad'
            $table->text('bicycle_desc');                       // Text column for description
            $table->decimal('bicycle_price', 6, 2);             // Decimal column for price with 6 digits and 2 decimal places
            $table->decimal('bicycle_price_commision', 6, 2);  // Decimal column for commission with 6 digits and 2 decimal places
            $table->timestamps();                               // Laravel's default timestamp columns (created_at and updated_at)
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('bicycles');
    }
};
