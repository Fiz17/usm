<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->string('product_id')->primary();
            $table->string('product_name', 50);             // Decimal column for price with 6 digits and 2 decimal places
            $table->text('product_desc');                      // Text column for description
            $table->decimal('product_price', 6, 2);         // Decimal column for commission with 6 digits and 2 decimal places
            $table->decimal('product_price_com', 6, 2);  // Decimal column for commission with 6 digits and 2 decimal places
            $table->integer('product_stock');  
            $table->integer('product_ini_stock');  
            $table->integer('product_sold')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};