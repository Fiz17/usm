<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('rentals', function (Blueprint $table) {
            $table->string('rental_id')->primary();
            $table->string('bicycle_id'); 
            $table->string('matric_id'); 
            $table->string('rent_name');
            $table->string('rent_matric_id');
            $table->string('rent_phone_num');
            $table->date('rent_start_date');
            $table->date('rent_end_date');
            $table->decimal('total_payment', 10, 2);
            $table->enum('status', ['pending', 'process', 'finish'])->default('pending');
            $table->timestamps();
            
            // Define foreign key constraints
            $table->foreign('bicycle_id')->references('bicycle_id')->on('bicycles');
            $table->foreign('matric_id')->references('matric_id')->on('users');
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rentals');
    }
};
