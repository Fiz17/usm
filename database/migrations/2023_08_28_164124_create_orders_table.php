<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->string('order_id')->primary();
            $table->string('product_id'); 
            $table->string('matric_id'); 
            $table->integer('order_quantity');
            $table->date('pickup_date');
            $table->decimal('total_order_payment', 10, 2);
            $table->enum('order_status', ['pending','finish'])->default('pending');
            $table->timestamps();
            
            // Define foreign key constraints
            $table->foreign('product_id')->references('product_id')->on('products');
            $table->foreign('matric_id')->references('matric_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
