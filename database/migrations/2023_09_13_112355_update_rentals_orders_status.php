<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('rentals', function (Blueprint $table) {
            $table->enum('status', ['pending', 'renting', 'returned'])->default('pending')->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->enum('order_status', ['pending', 'delivered'])->default('pending')->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('rentals', function (Blueprint $table) {
            $table->enum('status', ['pending', 'process', 'finish'])->default('pending')->change();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->enum('order_status', ['pending', 'finish'])->default('pending')->change();
        });
    }
};

