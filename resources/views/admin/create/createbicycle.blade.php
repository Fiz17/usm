@extends('template.dash')

@section('title')
    Add
@endsection

@section('style')

<style>

    .custom-card {
    background-color: #ffffff; /* Replace this with your desired soft grey color */
    }

</style>

@endsection
{{-- This section content for input new bicycle into database. --}}
@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h2>Add New Bicycle</h2>
    <a href="{{route ('bicycle.index')}}" class="btn btn-secondary"> Back</a>
</div>

<div class="card shadow-sm custom-card">
    <div class="card-header">
        <h5>Bicycle Form</h5>
    </div>
    <div class="card-body">
        {{-- Bicycle Form to create new bicycle into the database --}}
        <form action="{{route ('bicycle.store')}}" method="POST">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="bicycleID">Bicycle ID:</label>
                            <input type="text" class="form-control
                            @error('bicycle_id') 
                            is-invalid 
                            @enderror"
                            name="bicycle_id" value="{{old('bicycle_id')}}"> 
                            @error('bicycle_id') 
                                <span class="invalid-feedback"> {{$message}}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="bicycleCondition">Bicycle Condition:</label>
                            <select class="form-control" name="bicycle_condition" required>
                                <option value="good">Good</option>
                                <option value="bad">Bad</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="bicycleDescription">Bicycle Description:</label>
                            <textarea class="form-control
                            @error('bicycle_desc') 
                            is-invalid 
                            @enderror" 
                            name="bicycle_desc" value="{{old('bicycle_desc')}}" rows="3" ></textarea>
                            @error('bicycle_desc') 
                            <span class="invalid-feedback"> {{$message}}</span> 
                            @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="bicyclePrice">Price (RM):</label>
                            <input type="number" class="form-control" 
                            name="bicycle_price" value="{{old('bicycle_price')}}" step="0.1" min="0" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="bicycleCommission">Commission (RM):</label>
                            <input type="number" class="form-control" name="bicycle_price_commision" value="{{old('bicycle_price_commision')}}" step="0.1" min="0" >
                        </div>
                    </div>
                    <div class="col">
                        <a href="{{route ('bicycle.index')}}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
  </div>

@endsection