@extends('template.dash')

@section('title')
    Create
@endsection

@section('style')

<style>

    .custom-card {
    background-color: #ffffff; /* Replace this with your desired soft grey color */
}

</style>

@endsection

@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h2>Create New Product</h2>
    <a href="{{route('product.index')}}" class="btn btn-secondary"> Back</a>
</div>

<div class="card shadow-sm custom-card">
    <div class="card-header">
        <h5>Product Form</h5>
    </div>
    <div class="card-body">
        <form action="{{route ('product.store')}}" method="POST">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="productID">Product ID:</label>
                            <input type="text" class="form-control" id="productID" name="product_id" required>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="productName">Product Name:</label>
                            <input type="text" class="form-control" id="productName" name="product_name" required>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="productDescription">Product Description:</label>
                            <textarea class="form-control" id="productDescription" name="product_desc" rows="3" required></textarea>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="productPrice">Price (RM):</label>
                            <input type="number" class="form-control" id="productPrice" name="product_price" step="0.1" min="0" required>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="productCommission">Commission (RM):</label>
                            <input type="number" class="form-control" id="productCommission" name="product_price_com" step="0.1" min="0" required>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="productInitialStock">Initial Stock:</label>
                            <input type="number" class="form-control" id="productInitialStock" name="product_ini_stock" step="1" min="0" required>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group">
                            <label for="productStock">Current Stock:</label>
                            <input type="number" class="form-control" id="productStock" name="product_stock" step="1" min="0" required>
                        </div>
                    </div>
                    <div class="col">
                     <a href="{{route ('product.index')}}" class="btn btn-secondary">Cancel</a>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
  </div>

@endsection