@extends('template.dash')

@section('title')
    Create
@endsection

@section('style')

<style>

    .custom-card {
    background-color: #ffffff; /* Replace this with your desired soft grey color */
}

</style>

@endsection

@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h2>Create New Stock</h2>
    {{-- <a href="{{route('product.show', ['stock' => $stock_id])}}" class="btn btn-secondary"> Back</a> --}}
</div>

<div class="card shadow-sm custom-card">
    <div class="card-header">
        <h5>Stock Form</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('stock.store') }}" method="POST">
            @csrf
            <div class="container">
                {{-- <input type="hidden" name="product_id" value="{{ $product->product_id }}"> --}}
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="product_id">Product ID:</label>
                            <input type="text" class="form-control" id="product_id" name="product_id" value="{{ $product->product_id }}" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stock_id">Stock ID:</label>
                            <input type="text" class="form-control" id="stock_id" name="stock_id" value="{{ $stock_id }}" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stock_date">Stock Date:</label>
                            <input type="date" class="form-control" id="stock_date" name="stock_date" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="stock_quantity">Stock Quantity:</label>
                            <input class="form-control" id="stock_quantity" name="stock_quantity" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
  </div>

@endsection