@extends('template.dash')

@section('title')
    Upload
@endsection

@section('style')

<style>

    .custom-card {
    background-color: #ffffff; /* Replace this with your desired soft grey color */
}

</style>

@endsection

@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h2>Students Data</h2>
    <a href="{{route('useraccount.index')}}" class="btn btn-secondary"> Back</a>
</div>

<div class="card shadow-sm custom-card">
    <div class="card-header">
      <h5>Upload Data Form</h5>
      
    </div>
    <div class="card-body">
      <form action="{{ route('users.import') }}" method="POST" enctype="multipart/form-data">
      @csrf
        <div class="container mt-3">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="uploadExcel">Only accept excel type folder.</label>
                        <input type="file" class="form-control-file" id="uploadExcel" name="file" accept=".xlsx, .xls" required>
                    </div>
                </div>
                <div class="col mt-3">
                    <button type="upload" class="btn btn-primary">Upload</button>
                </div>
            </div>
        </div>
      </form>

      @if(session('error'))
        <script>
            window.onload = function() {
                alert("{{ session('error') }}");
            }
        </script>
        @endif

      @if(session('success'))
        <script>
            window.onload = function() {
                alert("{{ session('success') }}");
            }
        </script>
        @endif

      
    </div>
  </div>

  <!-- add print invoice button
    <div class="card-body">
        <form>
            <div class="container mt-3">
                <div class="row">
                    <div class="col mt-3">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5>Invoice Details</h5>
                                <a href="/invoice" class="btn btn-success" ><i class="fa fa-download"></i> Print invoice</a >
                        </div>
                    </div>
                </div>
            </div>
        </form> -->

@endsection