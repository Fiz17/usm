@extends('template.dash')

@section('content')
<div class="row">
    <div class="col">
        <div class="d-flex justify-content-end">
            <form action="{{route('adminaccount.destroy', $admin->id)}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this admin?');">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col-lg-8 col-12">
        <div class="container mt-4 mb-20">
            <div class="card">
                <div class="card-header">
                    Edit Admin Details
                </div>
                <div class="card-body">
                    <form action="{{route ('adminaccount.update', $admin->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- ADMIN ID (Read-only) -->
                        <div class="form-group">
                            <label for="id">Admin ID:</label>
                            <input type="text" class="form-control" id="id" name="id" value="{{ $admin->id }}" readonly>
                        </div>
            
                        <!-- Admin Email Address -->
                        <div class="form-group">
                            <label for="email">Admin Email:</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{ $admin->email}}" required>
                        </div>

                        <!-- Admin Type -->
                        <div class="form-group">
                            <label for="type">Admin Type:</label>
                            <select class="form-control" id="type" name="type" required>
                                <option value="master" {{ $admin->type === 'master' ? 'selected' : '' }}>master</option>
                                <option value="normal"  {{ $admin->type === 'normal' ? 'selected' : '' }}>normal</option>
                            </select>
                        </div>
            
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary mr-2">Save Changes</button>
                            <a href="{{route ('adminaccount.index')}}" class="btn btn-secondary"> Cancle</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-12">
        <div class="card mt-4">
            <div class="card-header">
                Dates
            </div>
            <div class="card-body">
                <p><strong>Created At:</strong><br> {{ $admin->created_at}}</p>
                <p><strong>Updated At:</strong><br> {{ $admin->updated_at}}</p>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    // Delete button click event
    document.getElementById('deleteButton').addEventListener('click', function() {
        // Show confirmation alert ""test comment
        var result = window.confirm('Are you sure you want to delete this admin account?');
        if (result) {
            // User clicked "OK" on the confirmation alert, perform the delete action
            // Add your delete logic here or redirect to a delete endpoint
            // For demo purposes, we'll just display an alert indicating the delete action
            alert('Admin deleted successfully!');

            window.location.href = "{{route ('adminaccount.index')}}";
        } else {
            // User clicked "Cancel" on the confirmation alert, do nothing
        }
    });
</script>
@endsection