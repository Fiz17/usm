@extends('template.dash')

@section('content')
<div class="row">
    <div class="col">
        <div class="d-flex justify-content-end">
            <form action="{{route('bicycle.destroy', $bicycle->bicycle_id)}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this bicycle?');">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col-lg-8 col-12">
        <div class="container mt-4 mb-20">
            <div class="card">
                <div class="card-header">
                    Edit Bicycle Details
                </div>
                <div class="card-body">
                    <form action="{{route ('bicycle.update', $bicycle->bicycle_id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- Bicycle ID (Read-only) -->
                        <div class="form-group">
                            <label for="bicycleID">Bicycle ID:</label>
                            <input type="text" class="form-control" id="bicycleID" name="bicycle_id" value="{{ $bicycle->bicycle_id }}" readonly>
                        </div>
            
                        <!-- Bicycle Condition -->
                        <div class="form-group">
                            <label for="bicycleCondition">Bicycle Condition:</label>
                            <select class="form-control" id="bicycleCondition" name="bicycle_condition" required>
                                <option value="good" {{ $bicycle->bicycle_condition === 'good' ? 'selected' : '' }}>Good</option>
                                <option value="bad"  {{ $bicycle->bicycle_condition === 'bad' ? 'selected' : '' }}>Bad</option>
                            </select>
                        </div>
            
                        <!-- Bicycle Description -->
                        <div class="form-group">
                            <label for="bicycleDescription">Bicycle Description:</label>
                            <textarea class="form-control" id="bicycleDescription" name="bicycle_desc" rows="3" required>{{ $bicycle->bicycle_desc}}</textarea>
                        </div>
            
                        <div class="row">
                            <!-- Bicycle Price -->
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="bicyclePrice">Price (RM):</label>
                                    <input type="number" class="form-control" id="bicyclePrice" name="bicycle_price" value="{{ $bicycle->bicycle_price}}" step="0.1" min="0" required>
                                </div>
                            </div>
                            
                            <!-- Bicycle Commission -->
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label for="bicycleCommission">Commission (RM):</label>
                                    <input type="number" class="form-control" id="bicycleCommission" name="bicycle_price_commision" value="{{ $bicycle->bicycle_price_commision}}" step="0.1" min="0" required>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <a href="{{route ('bicycle.index')}}" class="btn btn-secondary"> Cancel</a>
                            <button type="submit" class="btn btn-primary float-right ">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-12">
        <div class="card mt-4">
            <div class="card-header">
                Dates
            </div>
            <div class="card-body">
                <p><strong>Created At:</strong><br> {{ $bicycle->created_at}}</p>
                <p><strong>Updated At:</strong><br> {{ $bicycle->updated_at}}</p>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    // Delete button click event
    document.getElementById('deleteButton').addEventListener('click', function() {
        // Show confirmation alert ""test comment
        var result = window.confirm('Are you sure you want to delete this bicycle?');
        if (result) {
            // User clicked "OK" on the confirmation alert, perform the delete action
            // Add your delete logic here or redirect to a delete endpoint
            // For demo purposes, we'll just display an alert indicating the delete action
            alert('Bicycle deleted successfully!');

            window.location.href = "{{route ('bicycle.index')}}";
        } else {
            // User clicked "Cancel" on the confirmation alert, do nothing
        }
    });
</script>
@endsection