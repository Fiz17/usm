@extends('template.dash')

@section('content')
<div class="row">
    <div class="col">
        <div class="d-flex justify-content-end">
            <form action="{{route('order.destroy', $order->order_id)}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this order?');">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8 col-12">
        <div class="container mt-4 mb-20">
            <div class="card">
                <div class="card-header">
                    Edit Order Details
                </div>
                <div class="card-body">
                    <form action="{{route ('order.update', $order->order_id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- Rental ID (Read-only) -->
                        <div class="form-group">
                            <label for="order_id">Order ID:</label>
                            <input type="text" class="form-control" name="order_id" value="{{$order->order_id}}" readonly>
                        </div>

                        <!-- Hidden input field for product_id -->
                        <input type="hidden" name="product_id" value="{{ $order->product_id }}">

                        <!-- Bicycle ID (Read-only) -->
                        <div class="form-group">
                            <label for="bicycle_id">Product name:</label>
                            <input type="text" class="form-control" name="product_name" value="{{$order->product->product_name}}" readonly>
                        </div>

                        <!-- Name's Rental -->
                        <div class="form-group">
                            <label for="matric_id">Matric ID:</label>
                            <input type="text" class="form-control" name="matric_id" value="{{$order->matric_id}}" readonly>
                        </div>

                        <!-- Name's Rental -->
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" name="name" value="{{$order->user->name}}" readonly>
                        </div>
            
                        <!-- Matric ID's Rental -->
                        <div class="form-group">
                            <label for="order_quantity">Order quantity:</label>
                            <input type="text" class="form-control" name="order_quantity" value="{{$order->order_quantity}}" readonly>
                        </div>

                        <!-- Phone Number Rental -->
                        <div class="form-group">
                            <label for="pickup_date">Pickup Date:</label>
                            <input type="date" class="form-control" name="pickup_date" value="{{$order->pickup_date}}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="order_status">Status:</label>
                            <select class="form-control" name="order_status" value="{{ $order->order_status }}" required>
                                <option value="pending" {{ $order->order_status === 'pending' ? 'selected' : '' }}>Pending</option>
                                <option value="delivered"  {{ $order->order_status === 'delivered' ? 'selected' : '' }}>Delivered</option>
                            </select>
                        </div>

                        {{-- <!-- Start Date & End Date -->
                        <div class="row mb-3">
                        <div class="col">
                            <label for="rent_start_date" class="form-label">Start Date:</label>
                            <input type="date" class="form-control" name="rent_start_date" value="{{ $rental->rent_start_date}}" readonly>
                        </div>
                        <div class="col">
                            <label for="rent_end_date" class="form-label">End Date:</label>
                            <input type="date" class="form-control" name="rent_end_date"  value="{{ $rental->rent_end_date}}" readonly>
                        </div>
                        </div> --}}

                        <!-- Group -->
                        <div class="form-group">
                            <label for="group_num">Group:</label>
                            <input type="text" class="form-control"  name="group_num" value="{{ $order->user->group_num}}" readonly>
                        </div>

                        <!-- Price -->
                        <div class="form-group">
                            <label for="total_order_payment">Total Payment:</label>
                            <input type="text" class="form-control"  name="total_order_payment" value="{{ $order->total_order_payment}}" readonly>
                        </div>
    
                        <style>
                            .custom-card-body {
                                padding-left: 0;
                                padding-right: 0;
                            }
                        </style>
                        <div class="card-body custom-card-body">
                            <a href="{{route ('order.index', $order)}}" class="btn btn-secondary">Cancel</a>

                            <button type="submit" class="btn btn-primary float-right ml-2">Save Changes</button>
                            <a href="{{ route('orders.print', $order->order_id)}}" class="btn btn-success float-right" id="printButton" target="_blank">Print Receipt</a>
                            @csrf 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-12">
        <div class="card mt-4">
            <div class="card-header">
                Dates
            </div>
            <div class="card-body">
                <p><strong>Created At:</strong><br> {{ $order->created_at}}</p>
                <p><strong>Updated At:</strong><br> {{ $order->updated_at}}</p>
            </div>
        </div>
    </div>
</div>
@endsection