@extends('template.dash')

@section('content')
<div class="row">
    <div class="col">
        <div class="d-flex justify-content-end">
            <form action="{{route('product.destroy', $product->product_id)}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this product?');">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8 col-12">
        <div class="container mt-4 mb-20">
            <div class="card">
                <div class="card-header">
                    Edit Product Details
                </div>
                <div class="card-body">
                    <form action="{{route ('product.update', $product->product_id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- Product ID (Read-only) -->
                        <div class="form-group">
                            <label for="productID">Product ID:</label>
                            <input type="text" class="form-control" id="productID" name="product_id" value="{{ $product->product_id}}" readonly>
                        </div>
                        
                        <!-- Product Name -->
                        <div class="form-group">
                            <label for="productName">Product Name:</label>
                            <input type="text" class="form-control" id="productName" name="product_name" value="{{ $product->product_name}}" required>
                        </div>
        
                        <!-- Product Description -->
                        <div class="form-group">
                            <label for="productDescription">Product Description:</label>
                            <textarea class="form-control" id="productDescription" name="product_desc" rows="3" required>{{ $product->product_desc}}</textarea>
                        </div>
        
                        <div class="row">
                            <!-- Product Price -->
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="productPrice">Price (RM):</label>
                                <input type="number" class="form-control" id="productPrice" name="product_price" step="0.1" value="{{ $product->product_price}}" required>
                            </div>
                        </div>
                        
                        <!-- Product Commission -->
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="productCommission">Commission (RM):</label>
                                <input type="number" class="form-control" id="productCommission" name="product_price_com" step="0.1" value="{{ $product->product_price_com}}" required>
                            </div>
                        </div>

                        <!-- Product Initial Stock -->
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="productInitialStock">Initial Stock:</label>
                                <input type="number" class="form-control" id="productInitialStock" name="product_ini_stock" step="1" min="0" value="{{ $product->product_ini_stock}}" readonly>
                            </div>
                        </div>

                        <!-- Product Stock -->
                        <div class="col-12 col-lg-6">
                            <div class="form-group">
                                <label for="productStock">Current Stock:</label>
                                <input type="number" class="form-control" id="productStock" name="product_stock" step="1" min="0" value="{{ $product->product_stock}}" readonly>
                            </div>
                        </div>
                        </div>
                        <div class="card-body">
                            <a href="{{route ('product.index')}}" class="btn btn-secondary"> Cancel</a>
                            <button type="submit" class="btn btn-primary float-right ">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-12">
        <div class="card mt-4">
            <div class="card-header">
                Dates
            </div>
            <div class="card-body">
                <p><strong>Created At:</strong><br> {{ $product->created_at}}</p>
                <p><strong>Updated At:</strong><br> {{ $product->updated_at}}</p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    // Delete button click event
    document.getElementById('deleteButton').addEventListener('click', function() {
        // Show confirmation alert
        var result = window.confirm('Are you sure you want to delete this product?');
        if (result) {
            // User clicked "OK" on the confirmation alert, perform the delete action
            // Add your delete logic here or redirect to a delete endpoint
            // For demo purposes, we'll just display an alert indicating the delete action
            alert('Product deleted successfully!');
            
            window.location.href = "{{route ('product.index')}}";
        } else {
            // User clicked "Cancel" on the confirmation alert, do nothing
        }
    });
</script>
@endsection