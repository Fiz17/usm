@extends('template.dash')

@section('content')
<div class="row">
    <div class="col">
        <div class="d-flex justify-content-end">
            <form action="{{route('rental.destroy', $rental->rental_id)}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this rental?');">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-8 col-12">
        <div class="container mt-4 mb-20">
            <div class="card">
                <div class="card-header">
                    Edit Rental Details
                </div>
                <div class="card-body">
                    <form action="{{route ('rental.update', $rental->rental_id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- Rental ID (Read-only) -->
                        <div class="form-group">
                            <label for="rental_id">Rental ID:</label>
                            <input type="text" class="form-control" name="rental_id" value="{{ $rental->rental_id}}" readonly>
                        </div>

                        <!-- Bicycle ID (Read-only) -->
                        <div class="form-group">
                            <label for="bicycle_id">Bicycle ID:</label>
                            <input type="text" class="form-control" name="bicycle_id" value="{{ $rental->bicycle_id}}" readonly>
                        </div>

                        <!-- Name's Rental -->
                        <div class="form-group">
                            <label for="rent_name">Rental's name:</label>
                            <input type="text" class="form-control" name="rent_name" value="{{ $rental->rent_name }}" readonly>
                        </div>
            
                        <!-- Matric ID's Rental -->
                        <div class="form-group">
                            <label for="rent_matric_id">Rental's matric ID:</label>
                            <input type="text" class="form-control" name="rent_matric_id" value="{{ $rental->rent_matric_id}}" readonly>
                        </div>

                        <!-- Phone Number Rental -->
                        <div class="form-group">
                            <label for="rent_phone_num">Rental's Phone Number:</label>
                            <input type="text" class="form-control" name="rent_phone_num" value="{{ $rental->rent_phone_num}}" readonly>
                        </div>

                        <!-- Start Date & End Date -->
                        <div class="row mb-3">
                        <div class="col">
                            <label for="rent_start_date" class="form-label">Start Date:</label>
                            <input type="date" class="form-control" name="rent_start_date" value="{{ $rental->rent_start_date}}" readonly>
                        </div>
                        <div class="col">
                            <label for="rent_end_date" class="form-label">End Date:</label>
                            <input type="date" class="form-control" name="rent_end_date"  value="{{ $rental->rent_end_date}}" readonly>
                        </div>
                        </div>

                        <!-- Group -->
                        <div class="form-group">
                            <label for="group_num">Group:</label>
                            <input type="text" class="form-control"  name="group_num" value="{{ $rental->user->group_num }}" readonly>
                        </div>

                        <!-- Price -->
                        <div class="form-group">
                            <label for="total_payment">Total Payment:</label>
                            <input type="text" class="form-control"  name="total_payment" value="{{ $rental->total_payment }}" readonly>
                        </div>

                        <div class="form-group">
                            <label for="status">Status:</label>
                            <select class="form-control" name="status" value="{{ $rental->status }}" required>
                                <option value="pending" {{ $rental->status === 'pending' ? 'selected' : '' }}>Pending</option>
                                <option value="renting"  {{ $rental->status === 'renting' ? 'selected' : '' }}>Renting</option>
                                <option value="returned"  {{ $rental->status === 'returned' ? 'selected' : '' }}>Returned</option>
                            </select>
                        </div>
    
                        <div class="card-body">
                            <a href="{{route ('rental.index')}}" class="btn btn-secondary"> Cancel</a>

                            <button type="submit" class="btn btn-primary float-right ml-2">Save Changes</button>
                            <a href="{{ route('rentals.print', $rental->rental_id)}}" class="btn btn-success float-right" id="printButton" target="_blank">Print Receipt</a>
                            @csrf
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-12">
        <div class="card mt-4">
            <div class="card-header">
                Dates
            </div>
            <div class="card-body">
                <p><strong>Created At:</strong><br> {{ $rental->created_at}}</p>
                <p><strong>Updated At:</strong><br> {{ $rental->updated_at}}</p>
            </div>
        </div>
    </div>
</div>
@endsection