@extends('template.dash')

@section('content')
<div class="row">
    <div class="col">
        <div class="d-flex justify-content-end">
            <form action="{{route('useraccount.destroy', $user->matric_id)}}" method="POST" onsubmit="return confirm('Are you sure you want to delete this user?');">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        </div>
    </div>
</div>
    
<div class="row">
    <div class="col-lg-8 col-12">
        <div class="container mt-4 mb-20">
            <div class="card">
                <div class="card-header">
                    Edit User Details
                </div>
                <div class="card-body">
                    <form action="{{route ('useraccount.update', $user->matric_id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <!-- ADMIN ID (Read-only) -->
                        <div class="form-group">
                            <label for="matric_id">Matric ID:</label>
                            <input type="text" class="form-control" id="matric_id" name="matric_id" value="{{ $user->matric_id }}" required>
                        </div>

                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" required>
                        </div>

                        <div class="form-group">
                            <label for="group_num">Group Number:</label>
                            <input type="text" class="form-control" id="group_num" name="group_num" value="{{ $user->group_num }}" required>
                        </div>

                        <div class="form-group">
                            <label for="phone_num">Phone Number:</label>
                            <input type="text" class="form-control" id="phone_num" name="phone_num" value="{{ $user->phone_num }}" required>
                        </div>
            
                        <!-- Admin Email Address -->
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" required>
                        </div>

                        <!-- Admin Type -->
                        <div class="form-group">
                            <label for="status">Status:</label>
                            <input type="text" class="form-control" id="status" name="status" value="{{$user->status}}" readonly>
                        </div>
            
                        <div class="card-body">
                            <a href="{{route ('useraccount.index')}}" class="btn btn-secondary"> Cancel</a>
                            <button type="submit" class="btn btn-primary float-right">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-12">
        <div class="card mt-4">
            <div class="card-header">
                Dates
            </div>
            <div class="card-body">
                <p><strong>Created at:</strong><br> {{ $user->created_at}}</p>
                <p><strong>Updated at:</strong><br> {{ $user->updated_at}}</p>
                <p><strong>Verified at:</strong><br> 
                    {{ $user->email_verified_at ? $user->email_verified_at : 'Unverified' }}
                </p>
            </div>
        </div>
    </div>
</div>

@endsection

@section('script')
<script>
    // Delete button click event
    document.getElementById('deleteButton').addEventListener('click', function() {
        // Show confirmation alert ""test comment
        var result = window.confirm('Are you sure you want to delete this admin account?');
        if (result) {
            // User clicked "OK" on the confirmation alert, perform the delete action
            // Add your delete logic here or redirect to a delete endpoint
            // For demo purposes, we'll just display an alert indicating the delete action
            alert('Admin deleted successfully!');

            window.location.href = "{{route ('adminaccount.index')}}";
        } else {
            // User clicked "Cancel" on the confirmation alert, do nothing
        }
    });
</script>
@endsection