<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        @page {
            size: A4;
            margin: 0;
        }

        body {
    font-family: 'Roboto Condensed', sans-serif;
    }

    .border {
        background-color: white;
        width: 19cm;
        margin-left: 1cm;
        margin-top: 1cm;
        margin-bottom: 1cm;
        border-color: #42276a;
        border-width: 1px;
        border-style: solid;
        position: relative;
    }

    .parent-border {
        background-color: white;
        width: 21cm;
        height: auto;
        border-color: #42276a;
        border-width: 1px;
        border-style: solid;
    }

    .order-id {
        position: absolute;
        color: grey;
        top: -12px;
        left: 5px;
    }

    .table-row-border>th {
        border-left-color: white;
        border-right-color: white;
        border-bottom-color: #42276a;
        border-top-color: #42276a;
        border-width: 2px;
        border-style: solid;
        margin: 0;
        padding: 0;
        width: 4cm;
        height: 1cm;
        text-align: center;
        color: #42276a;
    }

    td {
        height: 1cm;
        text-align: center;
    }

    .table-addresses>th {
        color: #42276a;
        text-align: center;
        width: 8CM;
        text-align: left;
    }

    table.invoice-table-address {
        margin-left: 1cm;
    }

    table.invoice-table-address td {
        font-size: 15px;
        text-align: left;
        height: 0.5cm;
    }

    .parent {
        position: relative;
    }

    .child {
        position: absolute;
    }

    .invoice-table {
        margin-left: 1cm;
        margin-right: 1cm;
        /* border: teal 1px solid; */
    }

    .parent-invoice-logo-type {
        height: 3cm;
        /* border: teal 1px solid; */
    }

    .parent-invoice-table-address {
        margin-top: 1cm;
        height: 4cm;
        /* border: teal 1px solid; */
    }

    .parent-invoice-table {
        margin-top: 20px;
        /* border: teal 1px solid; */
    }

    .parent-invoice-total {
        margin-top: 1cm;
        /* border: teal 1px solid; */
        height: 1cm;
    }

    .parent-invoice-terms {
        margin-top: 4cm;
        /* border: teal 1px solid; */
        height: 5cm;
    }

    .invoice-type {
        font-size: 50px;
        font-weight: 700;
        color: #42276a;
    }

    .invoice-logo {
        right: 1cm;
        bottom: 0cm;
    }

    .invoice-total-text {
        font-size: 30px;
        font-weight: 700;
        color: #42276a;
        left: 1cm;
        bottom: 0cm;
    }

    .invoice-total {
        right: 1cm;
        bottom: 0cm;
        font-size: 30px;
        font-weight: 700;
    }

    .invoice-terms {
        left: 1cm;
        bottom: 0cm;
    }

    .recipient-sign {
        position: absolute;
        bottom: 1cm; 
        left: 1cm; 
    }

    .pic-sign {
        position: absolute;
        bottom: 1cm; 
        right: 2.5cm; 
    }

    .signature-lines {
        border-bottom: 1px solid #000;
        width: 5cm; 
        margin-bottom: 8px; 
    }
    </style>
</head>

<body>
    <div class="border">
        <div class="order-id">
            <p><strong>{{ $order->order_id }}</strong></p>
        </div>

        <div class="parent parent-invoice-logo-type">
            <span class="invoice-type" style="position: absolute; left: 1cm; bottom: 0cm;">
                RECEIPT
            </span>
            <span class="invoice-type" style="position: absolute; right: 1.5cm; bottom: 0cm;">
                WUS101
            </span>
        </div>



        <div class="parent parent-invoice-table-address">
            <table class="child invoice-table-address" style="border-spacing: 0;">
                <tr class="table-addresses">
                    <th>BILL TO</th>
                    <th>FROM</th>
                </tr>
                <tr class="temp">
                    <td>{{ $order->user->name }}</td>
                    <td>AKTIVITI KEUSAHAWANAN PELAJAR</td>
                    <td></td>
                </tr>
                <tr>
                    <td>{{ $order->matric_id }}</td>    
                    <td>UNTUK DAPATKAN PRODUK</td>
                </tr>
                <tr>
                    <td><strong>Group {{ $order->user->group_num }}</strong></td>
                    <td>DARIPADA <strong>{{ $order->product->product_name }}</strong></td> 
                </tr>
                <tr>
                    <td>+6{{ $order->user->phone_num }}</td>
                    <td>+604-6535243/5245/5249</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>PICKUP DATE: {{ $order->pickup_date }}</td>
                    <td><strong>ORDER ID: {{ $order->order_id }}</strong></p></td>
                </tr>
            </table>
        </div>

        <div class="parent parent-invoice-table">
            <table class="invoice-table" style="border-spacing: 0;">

                <tr class="table-row-border">
                    <th>GROUP</th>    
                    <th>PRODUCT</th>
                    <th>QUANTITY</th>
                    <th>UNIT PRICE</th>
                </tr>

                <tr>
                <td>{{ $order->user->group_num }}</td>
                <td>{{ $order->product->product_name }}</td>
                <td>{{ $order->order_quantity }}</td>
                <td >RM {{ $order->product->product_price }}</td>
                </tr>

            </table>

        </div>

        <div class="parent  parent-invoice-total">
            <span class="invoice-total-text child">
                TOTAL : RM {{ $order->total_order_payment}}
            </span>
        </div>

        <div class="parent  parent-invoice-terms">
            <div class="recipient-sign">
                <p><strong>RECIPIENT SIGNATURE:</strong></p>
                <p></p>
                <p></p>
                <div class="signature-lines"></div>
                <p class="uppercase-text">NAME: {{ $order->user->name }}</p>
                <p>DATE:  {{ $currentDate }}</p>
                </div>

            <div class="pic-sign">
                <p><strong>PIC SIGNATURE:</strong></p>
                <p></p>
                <p></p>
                <div class="signature-lines"></div>
                <p class="uppercase-text">NAME: {{ $adminName }}</p>
                <p>DATE: {{ $currentDate }}</p>
            </div>
        </div>
    </div>
    <script>
        // Function to get the current date in the format YYYY-MM-DD
        // function getCurrentDate() {
        //     const today = new Date();
        //     const year = today.getFullYear();
        //     const month = String(today.getMonth() + 1).padStart(2, '0');
        //     const day = String(today.getDate()).padStart(2, '0');
        //     return `${year}-${month}-${day}`;
        // }

        // document.getElementById('date').innerHTML=getCurrentDate();
        var dt = new Date();
        document.getElementById('date').textContent=dt.toDateString();
    </script>
</body>


</html>