@extends('template.dash')
{{-- This contents will only show from Master Admin Account Only --}}


@section('style')
<style>
    /* Custom CSS for Pending and Delivered Cards */

    .custom-card {
        border: none;
        border-radius: 10px;
        background-color: #ffffff;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .pending-card {
      background-color: #d4edda; /* Green */
      border-color: #c3e6cb; /* Soft Green */
    }
  
    .delivered-card {
      background-color:#f8d7da ;/* Red*/
        border-color:#f8d7da; /* Soft Red */
    }

    .card-text{
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;;
    }

    .card-title{
        font-size: 18px;
        font-weight: bold;
    }
</style>

@endsection

@section('content')
<div class="d-flex justify-content-between align-items-center">
  <h2>Admin Account Management</h2>
  <a href="{{route ('adminaccount.create')}}" class="btn btn-success" ><i class="fas fa-portrait"></i> Create Admin Account</a>
</div>
<div class="mt-4">
  <div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Search for admin...">
    <div class="input-group-append">
      <span class="input-group-text"><i class="fas fa-search"></i></span>
    </div>

    

    <div class="input-group-append ml-2">
      <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown">
        Filter
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">All</a>
        <a class="dropdown-item" href="#">Master</a>
        <a class="dropdown-item" href="#">Normal</a>
      </div>
    </div>
  </div>
  <table class="table table-bordered table-striped table-responsive-md">
    <thead>
      <tr>
        <th>@sortablelink('name', 'Name')</th>
        <th>@sortablelink('email', 'Admin Email')</th>
        <th>@sortablelink('type', 'Admin Type')</th>
        <th>Edit</th>
      </tr>
    </thead>
    <tbody>
      {{-- To display each admin database --}}
      @foreach($admins as $admin)
      <tr>
        <td>{{$admin->name}}</td>
        <td>{{$admin->email}}</td>
        <td>
            @if($admin->type === 'normal')
              <span class="badge badge-success">Normal</span>
            @else
              <span class="badge badge-info">Master</span>
            @endif
        </td>
        <td>
          {{-- {{dd($admin->id)}}; --}}
          <a href="{{ route('adminaccount.edit', $admin->id) }}" class="btn btn-sm btn-info"> Edit</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{-- Pagination (10 admin per page) --}}
  <div class="pagination-container">
    {{ $admins->appends(request()->except('page'))->links()}}
  </div>
</div>

@endsection