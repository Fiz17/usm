@extends('template.dash')

@section('style')

<style>
    /* Custom styling for cards */
    .custom-card {
        border: none;
        border-radius: 10px;
        background-color: #f8f9fa;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .custom-card .card-body {
        padding: 20px;
    }

    .custom-card .card-title {
        font-size: 18px;
        font-weight: bold;
    }

    .custom-card .card-text {
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;
    }
    
    /* Style for the first card */
    .first-card {
        background-color: #d4edda;
        border-color: rgb(195, 230, 203);
    }

    /* Style for the second card */
    .second-card {
        background-color:rgb(248, 215, 218) ;
        border-color:#f8d7da;
    }

    /* Style for the Third card */
    .third-card{
        background-color: rgb(220, 220, 255);
        border-color:rgb(220, 220, 255);
    }

    /* Badge size */
    .badge {
        font-size: 15px;
    }

    /* Pagination button to center */
    .pagination-container {
    display: flex;
    justify-content: center;
    margin-top: 20px;
    }
</style>

@endsection


@section('content')

<!--*****ALERT FEEDBACK*****-->
@if (session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
@endif

@if (session('info'))
    <div class="alert alert-info">{{session('info')}}</div>
@endif

@if (session('delete'))
    <div class="alert alert-danger">{{session('delete')}}</div>
@endif
<!-- ******************** -->

{{-- Header of Bicycle Page --}}
<div class="d-flex justify-content-between align-items-center mb-4">
    <h2>Bicycle Management</h2>
    <a href="{{route ('bicycle.create')}}" class="btn btn-success" ><i class="fa fa-dropbox"></i> New Bicycle</a>
</div>

{{-- This part is to display the cards for bicycle condition and the total bicycle --}}
<div class="row">
    <div class="col-md-4 mb-4">
        <div class="card custom-card first-card">
            <div class="card-body">
                <h5 class="card-title">Good Condition</h5>
                <p class="card-text">{{$goodCount}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="card custom-card second-card">
            <div class="card-body">
                <h5 class="card-title">Bad Condition</h5>
                <p class="card-text">{{$badCount}}</p>
            </div>
        </div>
    </div>
    <div class="col-md-4 mb-4">
        <div class="card custom-card third-card">
            <div class="card-body">
                <h5 class="card-title">Total Bicycle</h5>
                <p class="card-text">{{$allCount}}</p>
            </div>
        </div>
    </div>
</div>

{{-- Search Bar & Filter --}}
<div>
    <div class="input-group mb-3">
      <form action="{{ route('bicycle.search') }}" method="GET" class="input-group mb-3">
        <input type="text" name="query" class="form-control" placeholder="Search for bicycle..." value="{{ request('query') }}">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
          </div>
      </form>
    </div>
    <!-- <div class="input-group-append ml-2">
        <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown">Filter</button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#" data-filter="all">All</a>
            <a class="dropdown-item" href="#" data-filter="good">Good</a>
            <a class="dropdown-item" href="#" data-filter="bad">Bad</a>
        </div>
    </div> -->

{{-- Bicycle Table --}}
    @if (isset($searchResults))
        @if ($searchResults->isNotEmpty())
        <table class="table table-bordered table-striped table-responsive-md">
            <thead>
                <tr>
                    <th>@sortablelink('bicycle_id', 'Bicycle ID')</th>
                    <th>@sortablelink('bicycle_condition', 'Bicycle Condition')</th>
                    <th>Edit</th>
                </tr>
            </thead>

            <tbody>

                {{-- To display each search results --}}
                @foreach($searchResults as $bicycle)
                <tr>
                    <td>{{$bicycle->bicycle_id}}</td>
                    <td>
                        @if($bicycle->bicycle_condition === 'good')
                            <span class="badge badge-success">Good</span>
                        @else
                            <span class="badge badge-danger">Bad</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('bicycle.edit', $bicycle->bicycle_id)}}" class="btn btn-sm btn-info"> Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{-- Pagination (10 bicycle per page) --}}
        <div class="pagination-container">
            {{ $bicycles->appends(request()->except('page'))->links()}}
        </div>

        @else
            <div class="alert alert-warning">No search results found.</div>
        @endif
    @else
    <table class="table table-bordered table-striped table-responsive-md">
        <thead>
            <tr>
                <th>@sortablelink('bicycle_id', 'Bicycle ID')</th>
                <th>@sortablelink('bicycle_condition', 'Bicycle Condition')</th>
                <th>Edit</th>
            </tr>
        </thead>

        <tbody>

            {{-- To display each bicycle database --}}
            @foreach($bicycles as $bicycle)
            <tr>
                <td>{{$bicycle->bicycle_id}}</td>
                <td>
                    @if($bicycle->bicycle_condition === 'good')
                        <span class="badge badge-success">Good</span>
                    @else
                        <span class="badge badge-danger">Bad</span>
                    @endif
                </td>
                <td>
                    <a href="{{route('bicycle.edit', $bicycle->bicycle_id)}}" class="btn btn-sm btn-info"> Edit</a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>

    {{-- Pagination (10 bicycle per page) --}}
    <div class="pagination-container">
        {{ $bicycles->appends(request()->except('page'))->links()}}
    </div>
    @endif
</div>

@endsection