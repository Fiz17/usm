@extends('template.dash')

@section('style')
<style>
    .custom-col-xl {
    flex-basis: calc(20% * 2.4);
    max-width: calc(20% * 2.4);
}
</style>

@endsection

@section('content')

<!-- Page Heading -->
<div class="d-flex justify-content-between align-items-center mb-4">
    <h2>Dashboard</h2>
    <a href="{{ route('weeklyreport.export') }}" class="btn btn-success"><i class="fas fa-download"></i> Generate Report</a>
    @if(session('error'))
    <script>
        window.onload = function() {
            alert("{{ session('error') }}");
        }
    </script>
    @endif
</div>
    
<!-- Content Row -->
<div class="row" style="justify-content: space-between;">
    <!--  Users Account Created -->
    <div class="col-lg-2 col-md-3 mr-4 mb-4">
        <div class="card border-left-primary shadow h-100 py-2" id="card-id">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"> 
                            Account Created
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            {{$accountCreate}}
                        </div>
                    </div>
                        <div class="col-auto">
                            <i class="fas fa-users fa-2x text-gray-300"></i>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Pending Order -->
    <div class="col-lg-2 col-md-3 mr-4 mb-4">
        <div class="card border-left-warning shadow h-100 py-2" id="card-id">
            <a href="#" class="card-link">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Pending Order</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{$pendingOrder}}
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <!--  Rental -->
    <div class="col-lg-2 col-md-3 mr-4 mb-4">
        <div class="card border-left-info shadow h-100 py-2" id="card-id">
            <a href="#" class="card-link">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                Pending Rental</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                {{$pendingRental}}
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>


<!--  Rental Revenue -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-success shadow h-100 py-2" id="card-id">
        <a href="#" class="card-link">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                            Rental Revenue (RM)</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                            0
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
</div>

<!-- Latest Orders and Rentals -->
<h4>Latest Order</h4>
<div class="card mb-5">
    <table class="table table-bordered ">
        <thead>
            <tr>
                <th>Group</th>
                <th>Order ID</th>
                <th>Product Name</th>
                <th>Order Quantity</th>
                <th>Pickup Date</th>
                <th>Total Order Price (RM)</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($orders as $order)
            <tr>
                <td>{{$order->user->group_num}}</td>
                <td>{{$order->order_id}}</td>
                <td>{{$order->product->product_name}}</td>
                <td>{{$order->order_quantity}}</td>
                <td>{{$order->pickup_date}}</td>
                <td>{{$order->total_order_payment}}</td>
                <td>
                @if($order->order_status === 'pending')
                    <span class="badge badge-warning">Pending</span>
                @elseif($order->order_status === 'delivered')
                    <span class="badge badge-success">Delivered</span>
                @endif
                </td>
            </tr>
                @endforeach
        </tbody>
    </table>
</div>
    
<!-- Latest Rentel Bicycles -->
<h4>Latest Rental Bicycles</h4>
<div class="card">
    <table class="table table-bordered ">
        <thead>
            <tr>
                <th>Group</th>
                <th>Rental ID</th>
                <th>Bicycle ID</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rentals as $rental)
            <tr>
                <td>{{$rental->user->group_num}}</td>
                <td>{{$rental->rental_id}}</td>
                <td>{{$rental->bicycle_id}}</td>
                <td>{{$rental->rent_start_date}}</td>
                <td>{{$rental->rent_end_date}}</td>
                <td>
                    @if($rental->status === 'pending')
                        <span class="badge badge-warning">Pending</span>
                    @elseif($rental->status === 'renting')
                        <span class="badge badge-danger">Renting</span>
                    @elseif($rental->status === 'returned')
                        <span class="badge badge-success">Returned</span>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection