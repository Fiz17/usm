@extends('template.dash')

@section('style')
<style>
    /* Custom CSS for Pending and Delivered Cards */

    .custom-card {
        border: none;
        border-radius: 10px;
        background-color: #ffffff;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .pending-card {
      background-color: #fff3cd; /* Yellow */
      border-color: #ffeeba; /* Soft Yellow */
    }
  
    .delivered-card {
      background-color: #d4edda; /* Green */
      border-color: #c3e6cb; /* Soft Green */
    }

    .card-text{
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;;
    }

    .card-title{
        font-size: 18px;
        font-weight: bold;
    }
</style>
@endsection


@section('content')
<!--*****ALERT FEEDBACK*****-->

@if (session('info'))
    <div class="alert alert-info">{{session('info')}}</div>
@endif

@if (session('delete'))
    <div class="alert alert-danger">{{session('delete')}}</div>
@endif
<!-- ******************** -->

<div class="d-flex justify-content-between align-items-center">
  <h2>Orders</h2>

  <div class = "d-flex">
  <a href="{{ route('pendingorder.reminder') }}" class="btn btn-danger mr-4">Reminder</a>
  @if(session('success'))
  <script>
      window.onload = function() {
          alert("{{ session('success') }}");
      }
  </script>
  @endif
  @if(session('error'))
  <script>
      window.onload = function() {
          alert("{{ session('error') }}");
      }
  </script>
  @endif
</div>
</div>
  <div class="row mt-4">
    <div class="col-md-6 mb-4">
      <div class="card shadow-sm custom-card pending-card">
        <div class="card-body">
          <h5 class="card-title">Pending Orders</h5>
          <p class="card-text">{{$pendingCount}}</p>
        </div>
      </div>
    </div>
    <div class="col-md-6 mb-4">
      <div class="card shadow-sm custom-card delivered-card">
        <div class="card-body">
          <h5 class="card-title">Delivered Orders</h5>
          <p class="card-text">{{$finishCount}}</p>
        </div>
      </div>
    </div>
  </div>
  <div class="mt-4">
    <div class="input-group mb-3">
      <form action="{{ route('order.search') }}" method="GET" class="input-group mb-3">
        <input type="text" name="query" class="form-control" placeholder="Search for order..." value="{{ request('query') }}">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
          </div>
      </form>
      <!-- <div class="input-group-append ml-2">
        <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown">
          Filter
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">All</a>
          <a class="dropdown-item" href="#">Pending</a>
          <a class="dropdown-item" href="#">Delivered</a>
        </div>
      </div> -->
    </div>
    @if (isset($searchResults))
      @if ($searchResults->isNotEmpty())
        <table class="table table-bordered table-striped table-responsive-md">
        <thead>
          <tr>
            <th>@sortablelink('user.group_num', 'Group')</th>
            <th>@sortablelink('order_id', 'Order ID')</th>
            <th>@sortablelink('product.product_name', 'Product Name')</th>
            <th>@sortablelink('order_quantity', 'Quantity')</th>
            <th>@sortablelink('pickup_date', 'Pickup Date')</th>
            <th>@sortablelink('total_order_payment', 'Total Price (RM)')</th>
            <th>@sortablelink('status', 'Status')</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($searchResults as $order)
          <tr>
            <td>{{$order->user->group_num}}</td>
            <td>{{$order->order_id}}</td>
            <td>{{$order->product->product_name}}</td>
            <td>{{$order->order_quantity}}</td>
            <td>{{$order->pickup_date}}</td>
            <td>{{$order->total_order_payment}}</td>
            <td>
              @if($order->order_status === 'pending')
                <span class="badge badge-warning">Pending</span>
              @elseif($order->order_status === 'delivered')
                <span class="badge badge-success">Delivered</span>
              @endif
            </td>
            <td>
              <a href="{{route('order.edit', $order->order_id)}}" class="btn btn-sm btn-info"> Edit</a>
            </td>
          </tr>
          @endforeach
        </tbody>
        </table>
        {{-- Pagination (10 Order per page) --}}
        <div class="pagination-container">
          {{ $searchResults->appends(request()->except('page'))->links()}}
        </div>
      @else
        <div class="alert alert-warning">No search results found.</div>
      @endif
    @else
        <table class="table table-bordered table-striped table-responsive-md">
          <thead>
            <tr>
              <th>@sortablelink('user.group_num', 'Group')</th>
              <th>@sortablelink('order_id', 'Order ID')</th>
              <th>@sortablelink('product.product_name', 'Product Name')</th>
              <th>@sortablelink('order_quantity', 'Quantity')</th>
              <th>@sortablelink('pickup_date', 'Pickup Date')</th>
              <th>@sortablelink('total_order_payment', 'Total Price (RM)')</th>
              <th>@sortablelink('status', 'Status')</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($orders as $order)
            <tr>
              <td>{{$order->user->group_num}}</td>
              <td>{{$order->order_id}}</td>
              <td>{{$order->product->product_name}}</td>
              <td>{{$order->order_quantity}}</td>
              <td>{{$order->pickup_date}}</td>
              <td>{{$order->total_order_payment}}</td>
              <td>
                @if($order->order_status === 'pending')
                  <span class="badge badge-warning">Pending</span>
                @elseif($order->order_status === 'delivered')
                  <span class="badge badge-success">Delivered</span>
                @endif
              </td>
              <td>
                <a href="{{route('order.edit', $order->order_id)}}" class="btn btn-sm btn-info"> Edit</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{-- Pagination (10 Order per page) --}}
        <div class="pagination-container">
          {{ $orders->appends(request()->except('page'))->links()}}
        </div>
      @endif
  </div>

@endsection