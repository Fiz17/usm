@extends('template.dash')

@section('style')

<style>
    /* Custom styling for cards */
    .custom-card {
        border: none;
        border-radius: 10px;
        background-color: #f8f9fa;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .custom-card .card-body {
        padding: 20px;
    }

    .custom-card .card-title {
        font-size: 18px;
        font-weight: bold;
    }

    .custom-card .card-text {
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;
    }
    
    /* Style for the first card */
    .first-card {
        background-color: #d4edda; /* Green */
        border-color: #c3e6cb; /* Soft Green */
    }

    /* Style for the second card */
    .second-card {
        background-color:#f8d7da ;/* Red*/
        border-color:#f8d7da; /* Soft Red */
    }

    /* Pagination button to center */
    .pagination-container {
    display: flex;
    justify-content: center;
    margin-top: 20px;
    }
</style>

@endsection


@section('content')

<!--*****ALERT FEEDBACK*****-->
@if (session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
@endif

@if (session('info'))
    <div class="alert alert-info">{{session('info')}}</div>
@endif

@if (session('delete'))
    <div class="alert alert-danger">{{session('delete')}}</div>
@endif
<!-- ******************** -->

<div class="d-flex justify-content-between align-items-center mb-4">
    <h2>Product Management</h2>
    <a href="{{route ('product.create')}}" class="btn btn-success" ><i class="fa fa-dropbox"></i> New Product</a>
</div>

<div class="row">
    <div class="col-md-6 mb-4">
        <div class="card custom-card first-card">
            <div class="card-body">
                <h5 class="card-title">Products In Stock</h5>
                <p class="card-text">{{$totalStock}}</p>
            </div>
        </div>
    </div>
    
    <div class="col-md-6 mb-4">
        <div class="card custom-card second-card">
            <div class="card-body">
                <h5 class="card-title">Products Sold</h5>
                <p class="card-text">{{$totalSold}}</p>
            </div>
        </div>
    </div>
</div>

<div class="mt-4">
    <div class="input-group mb-3">
      <form action="{{ route('product.search') }}" method="GET" class="input-group mb-3">
        <input type="text" name="query" class="form-control" placeholder="Search for product..." value="{{ request('query') }}">
          <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
          </div>
      </form>
    </div>
    @if (isset($searchResults))
        @if ($searchResults->isNotEmpty())
        <table class="table table-bordered table-striped table-responsive-md">
            <thead>
                <tr>
                    <th>@sortablelink('product_id', 'Product ID')</th>
                    <th>@sortablelink('product_name', 'Name')</th>
                    <th>@sortablelink('product_stock', 'Product Stock')</th>
                    <th>Manage Stock</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($searchResults as $product)
                <tr>
                    <td>{{$product->product_id}}</td>
                    <td>{{$product->product_name}}</td>
                    <td>{{$product->product_stock}}</td>
                    <td>
                        <a href="{{route('product.show' , $product->product_id)}}" class="btn btn-sm btn-primary"> Stock</a>
                    </td>
                    <td>
                        <a href="{{route('product.edit', $product->product_id)}}" class="btn btn-sm btn-info"> Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        {{-- Pagination (10 bicycle per page) --}}
        <div class="pagination-container">
            {{ $products->appends(request()->except('page'))->links()}}
        </div>
        @else
            <div class="alert alert-warning">No search results found.</div>
        @endif
    @else
    <table class="table table-bordered table-striped table-responsive-md">
            <thead>
                <tr>
                    <th>@sortablelink('product_id', 'Product ID')</th>
                    <th>@sortablelink('product_name', 'Name')</th>
                    <th>@sortablelink('product_stock', 'Product Stock')</th>
                    <th>Manage Stock</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $product)
                <tr>
                    <td>{{$product->product_id}}</td>
                    <td>{{$product->product_name}}</td>
                    <td>{{$product->product_stock}}</td>
                    <td>
                        <a href="{{route('product.show' , $product->product_id)}}" class="btn btn-sm btn-primary"> Stock</a>
                    </td>
                    <td>
                        <a href="{{route('product.edit', $product->product_id)}}" class="btn btn-sm btn-info"> Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        
        {{-- Pagination (10 bicycle per page) --}}
        <div class="pagination-container">
            {{ $products->appends(request()->except('page'))->links()}}
        </div>
    @endif
</div>

@endsection