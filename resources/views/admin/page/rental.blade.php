@extends('template.dash')

@section('style')
    
<style>

    /* Define the styles for the rental link */
    .linker-link {
    color: #5e5b5b; /* Set the desired text color (black in this case) */
    text-decoration: none; /* Remove the default underline */
    }

    /* Define the style when the link is hovered (optional) */
    .linker:hover {
    text-decoration: none; /* Add an underline on hover if desired */
    color: #5e5b5b;
    }

    .linker{
      color: #5e5b5b;
    }
    
    /* ============================== */

    /* Custom CSS for Pending and Delivered Cards */
    .pending-card {
      background-color: #fff3cd; /* Yellow */
      border-color: #ffeeba; /* Soft Yellow */
    }
  
    .in-card{
        background-color:#f8d7da ;/* Red*/
        border-color:#f8d7da; /* Soft Red */
    }

    .delivered-card {
      background-color: #d4edda; /* Green */
      border-color: #c3e6cb; /* Soft Green */
    }

    .card-text{
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;
    }

    .card-title{
        font-size: 18px;
        font-weight: bold;
    }
</style>

@endsection


@section('content')
<!--*****ALERT FEEDBACK*****-->

@if (session('info'))
    <div class="alert alert-info">{{session('info')}}</div>
@endif

@if (session('delete'))
    <div class="alert alert-danger">{{session('delete')}}</div>
@endif
<!-- ******************** -->

<div class="d-flex justify-content-between align-items-center">
  <h2>Rentals</h2>

  <div class = "d-flex">
    <a href="{{ route('returnbicycle.reminder') }}" class="btn btn-danger mr-4">Reminder</a>
    @if(session('success'))
    <script>
        window.onload = function() {
            alert("{{ session('success') }}");
        }
    </script>
    @endif
    @if(session('error'))
    <script>
        window.onload = function() {
            alert("{{ session('error') }}");
        }
    </script>
    @endif
  </div>
</div>
<div class="row mt-4">
  <div class="col-md-4 mb-4">
    <div class="card shadow-sm pending-card">
      <div class="card-body">
        <h5 class="card-title">Pending Rental</h5>
        <p class="card-text">{{$pendingCount}}</p>
      </div>
    </div>
  </div>

  <div class="col-md-4 mb-4">
    <div class="card shadow-sm in-card">
      <div class="card-body">
        <h5 class="card-title">Currently Renting</h5>
        <p class="card-text">{{$processCount}}</p>
      </div>
    </div>
  </div>

  <div class="col-md-4 mb-4">
    <div class="card shadow-sm delivered-card">
      <div class="card-body">
        <h5 class="card-title">Returned Rental</h5>
        <p class="card-text">{{$finishCount}}</p>
      </div>
    </div>
  </div>
</div>

<!-- ******Table Body****** -->
<div class="mt-4">
  <div class="input-group mb-3">
    <form action="{{ route('rental.search') }}" method="GET" class="input-group mb-3">
      <input type="text" name="query" class="form-control" placeholder="Search for rental..." value="{{ request('query') }}">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
        </div>
    </form>
    <!-- <div class="input-group-append ml-2">
      <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown">
        Filter
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Pending</a>
        <a class="dropdown-item" href="#">Process</a>
        <a class="dropdown-item" href="#">Finish</a>
      </div>-->
  </div>
  @if (isset($searchResults))
    @if ($searchResults->isNotEmpty())
    <table class="table table-bordered table-striped table-responsive-md">
      <thead>
        <tr>
          <th>@sortablelink('user.group_num', 'Group')</th>
          <th>@sortablelink('rental_id', 'Rental ID')</th>
          <th>@sortablelink('bicycle_id', 'Bicycle ID')</th>
          <th>@sortablelink('rent_start_date', 'Start Date')</th>
          <th>@sortablelink('rent_end_date', 'End Date')</th>
          <th>@sortablelink('total_payment', 'Total Payment (RM)')</th>
          <th>@sortablelink('status', 'Status')</th>
          <th>Edit</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($searchResults as $rental)
        <tr>
          <td>{{$rental->user->group_num}}</td>
          <td>{{$rental->rental_id}}</td>
          <td>{{$rental->bicycle_id}}</td>
          <td>{{$rental->rent_start_date}}</td>
          <td>{{$rental->rent_end_date}}</td>
          <td>{{$rental->total_payment}}</td>
          <td>
            @if($rental->status === 'pending')
                <span class="badge badge-warning">Pending</span>
            @elseif($rental->status === 'renting')
                <span class="badge badge-danger">Renting</span>
            @elseif($rental->status === 'returned')
                <span class="badge badge-success">Returned</span>
            @endif
          </td>
          <td>
            <a href="{{route('rental.edit', $rental->rental_id)}}" class="btn btn-sm btn-info"> Edit</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>

    {{-- Pagination (10 Rental per page) --}}
    <div class="pagination-container">
      {{ $rentals->appends(request()->except('page'))->links()}}
    </div>

    @else
      <div class="alert alert-warning">No search results found.</div>
    @endif

  @else
  <table class="table table-bordered table-striped table-responsive-md">
    <thead>
      <tr>
        <th>@sortablelink('user.group_num', 'Group')</th>
        <th>@sortablelink('rental_id', 'Rental ID')</th>
        <th>@sortablelink('bicycle_id', 'Bicycle ID')</th>
        <th>@sortablelink('rent_start_date', 'Start Date')</th>
        <th>@sortablelink('rent_end_date', 'End Date')</th>
        <th>@sortablelink('total_payment', 'Total Payment (RM)')</th>
        <th>@sortablelink('status', 'Status')</th>
        <th>Edit</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($rentals as $rental)
      <tr>
        <td>{{$rental->user->group_num}}</td>
        <td>{{$rental->rental_id}}</td>
        <td>{{$rental->bicycle_id}}</td>
        <td>{{$rental->rent_start_date}}</td>
        <td>{{$rental->rent_end_date}}</td>
        <td>{{$rental->total_payment}}</td>
        <td>
          @if($rental->status === 'pending')
              <span class="badge badge-warning">Pending</span>
          @elseif($rental->status === 'renting')
              <span class="badge badge-danger">Renting</span>
          @elseif($rental->status === 'returned')
              <span class="badge badge-success">Returned</span>
          @endif
      </td>
        <td>
          <a href="{{route('rental.edit', $rental->rental_id)}}" class="btn btn-sm btn-info"> Edit</a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>

  {{-- Pagination (10 Rental per page) --}}
  <div class="pagination-container">
    {{ $rentals->appends(request()->except('page'))->links()}}
  </div>

  @endif
</div>

@endsection