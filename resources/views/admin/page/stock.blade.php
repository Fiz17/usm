@extends('template.dash')

@section('style')

<style>
    /* Custom styling for cards */
    .custom-card {
        border: none;
        border-radius: 10px;
        background-color: #f8f9fa;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    .custom-card .card-body {
        padding: 20px;
    }

    .custom-card .card-title {
        font-size: 18px;
        font-weight: bold;
    }

    .custom-card .card-text {
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;
    }
    
    /* Style for the first card */
    .first-card {
        background-color: #d4edda; /* Green */
        border-color: #c3e6cb; /* Soft Green */
    }

    /* Style for the second card */
    .second-card {
        background-color:#9ceafd ;/* blue*/
        border-color:#9ceafd; /* Soft blue */
    }

    /* Pagination button to center */
    .pagination-container {
    display: flex;
    justify-content: center;
    margin-top: 20px;
    }
</style>

@endsection


@section('content')

<!--*****ALERT FEEDBACK*****-->
@if (session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
@endif

@if (session('info'))
    <div class="alert alert-info">{{session('info')}}</div>
@endif

@if (session('delete'))
    <div class="alert alert-danger">{{session('delete')}}</div>
@endif
<!-- ******************** -->

<div class="d-flex justify-content-between align-items-center mb-4">
    <h2>Stock Management</h2>
    <a href="{{route('stock.show' , $product->product_id)}}" class="btn btn-success" ><i class="fa fa-dropbox"></i> New Stock </a>
    {{-- <a href="{{route ('stock.create', ['product_id' => $product->product_id] )}}" class="btn btn-success" ><i class="fa fa-dropbox"></i> New Stock </a> --}}
</div>

<div class="row">
    <div class="col-md-6 mb-4">
        <div class="card custom-card first-card">
            <div class="card-body">
                <h5 class="card-title">Products in Stock:</h5>
                <p class="card-text">{{$product_stock}}</p>
            </div>
        </div>
    </div>
    
    <div class="col-md-6 mb-4">
        <div class="card custom-card second-card">
            <div class="card-body">
                <h5 class="card-title">Products Initial Stock:</h5>
                <p class="card-text">{{$product_ini_stock}}</p>
            </div>
        </div>
    </div>
</div>

<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>@sortablelink('stock_id', 'Stock ID')</th>
                <th>@sortablelink('stock_quantity', 'Quantity')</th>
                <th>@sortablelink('stock_date', 'Stock Received')</th>
                <th>Edit</th>
            </tr>
        </thead>
        <tbody>
            @foreach($stocks as $stock)
            <tr>
                <td>{{$stock->stock_id}}</td>
                <td>{{$stock->stock_quantity}}</td>
                <td>{{$stock->stock_date}}</td>
                <td>
                    <form action="{{route('stock.destroy', $stock->stock_id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="hidden" name="product_id" value="{{ $stock->product_id }}">
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                    {{-- <a href="{{route('stock.edit', $stock->stock_id)}}" class="btn btn-sm btn-info"> Edit</a> --}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    
    {{-- Pagination (10 bicycle per page) --}}
    <div class="pagination-container">
        {{ $stocks->appends(request()->except('page'))->links()}}
    </div>
</div>

@endsection