@extends('template.dash')

@section('style')
    {{--  --}}
<style>
    /* Custom CSS for Pending and Delivered Cards */

    /* Custom CSS for Pending and Delivered Cards */
    .unregisted-card {
      background-color: #fff3cd; /* Yellow */
      border-color: #ffeeba; /* Soft Yellow */
    }
  
    .deleted-card{
        background-color:#f8d7da ;/* Red*/
        border-color:#f8d7da; /* Soft Red */
    }

    .registed-card {
      background-color: #d4edda; /* Green */
      border-color: #c3e6cb; /* Soft Green */
    }

    .card-text{
        font-size: 24px;
        font-weight: bold;
        color: #5e5b5b;;
    }

    .card-title{
        font-size: 18px;
        font-weight: bold;
    }
</style>

@endsection


@section('content')
<div class="d-flex justify-content-between align-items-center">
        <h2>User Account Management</h2>
  
<div class = "d-flex">
  <a href="{{ route('register.reminder') }}" class="btn btn-danger mr-4">Reminder</a>
  @if(session('success'))
  <script>
      window.onload = function() {
          alert("{{ session('success') }}");
      }
  </script>
  @endif
  @if(session('error'))
  <script>
      window.onload = function() {
          alert("{{ session('error') }}");
      }
  </script>
  @endif

  <a href="{{ route('useraccount.create') }}" class="btn btn-success" ><i class="fa fa-upload"></i> Upload Students Data</a>
</div>
</div>
    <div class="row mt-4">
      <div class="col-md-6 mb-4">
        <div class="card shadow-sm unregisted-card">
          <div class="card-body">
            <h5 class="card-title">Not Registered</h5>
            <p class="card-text">{{$unverified}}</p>
          </div>
        </div>
      </div>
      <div class="col-md-6 mb-4">
          <div class="card shadow-sm registed-card">
            <div class="card-body">
              <h5 class="card-title">Already Registered</h5>
              <p class="card-text">{{$verified}}</p>
            </div>
          </div>
        </div>
    </div>
  <div class="mt-4">
    <div class="input-group mb-3">
      <form action="{{ route('user.search') }}" method="GET" class="input-group mb-3">
          <input type="text" name="query" class="form-control" placeholder="Search for user..." value="{{ request('query') }}">
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="submit"><i class="fas fa-search"></i></button>
            </div>
      </form>
      <!-- <div class="input-group-append ml-2">
        <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown">
          Filter
        </button>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="#">All</a>
          <a class="dropdown-item" href="#">Unregisted</a>
          <a class="dropdown-item" href="#">Registed</a>
        </div>
      </div> -->
    </div>
    @if (isset($searchResults))
      @if ($searchResults->isNotEmpty())
        <table class="table table-bordered table-striped table-responsive-md">
        <thead>
          <tr>
            <th>@sortablelink('group_num', 'Group number')</th>
            <th>@sortablelink('matric_id', 'Matric ID')</th>
            <th>@sortablelink('name', 'Name')</th>
            <th>@sortablelink('email', 'Email Address')</th>
            <th>@sortablelink('phone_num', 'Phone Number')</th>
            <th>@sortablelink('status', 'Status')</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          
          @foreach($searchResults as $user)
          <tr>
            <td>{{$user->group_num}}</td>
            <td>{{$user->matric_id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone_num}}</td>
            <td>
              @if($user->status === 'unverified')
              <span class="badge badge-warning">Unverified</span>
              @else
              <span class="badge badge-success">Verified</span>
              @endif
            </td>
            <td><a href="{{ route('useraccount.edit', $user->matric_id) }}" class="btn btn-sm btn-info">Edit</a></td>
          </tr>
          @endforeach
        </tbody>
        </table>
        {{-- Pagination (10 admin per page) --}}
        <div class="pagination-container">
          {{ $users->appends(request()->except('page'))->links()}}
        </div>
      @else
        <div class="alert alert-warning">No search results found.</div>
      @endif
    @else
      <table class="table table-bordered table-striped table-responsive-md">
        <thead>
          <tr>
            <th>@sortablelink('group_num', 'Group number')</th>
            <th>@sortablelink('matric_id', 'Matric ID')</th>
            <th>@sortablelink('name', 'Name')</th>
            <th>@sortablelink('email', 'Email Address')</th>
            <th>@sortablelink('phone_num', 'Phone Number')</th>
            <th>@sortablelink('status', 'Status')</th>
            <th>Edit</th>
          </tr>
        </thead>
        <tbody>
          
          @foreach($users as $user)
          <tr>
            <td>{{$user->group_num}}</td>
            <td>{{$user->matric_id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone_num}}</td>
            <td>
              @if($user->status === 'unverified')
              <span class="badge badge-warning">Unverified</span>
              @else
              <span class="badge badge-success">Verified</span>
              @endif
            </td>
            <td><a href="{{ route('useraccount.edit', $user->matric_id) }}" class="btn btn-sm btn-info">Edit</a></td>
          </tr>
          @endforeach
        </tbody>
        </table>
        {{-- Pagination (10 admin per page) --}}
        <div class="pagination-container">
          {{ $users->appends(request()->except('page'))->links()}}
        </div>
    @endif
  </div>

@endsection