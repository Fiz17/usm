<x-guest-layout>
    <form method="POST" action="{{ route('register') }}">
        @csrf

        <!-- Matric ID -->
        <div>
            <x-input-label for="matric_id" :value="__('Matric ID')" />
            <x-text-input id="matric_id" class="block mt-1 w-full" type="text" name="matric_id" :value="old('matric_id')" required autofocus />
            <x-input-error :messages="$errors->get('matric_id')" class="mt-2" />
        </div>

        <!-- Name -->
        <div class="mt-4">
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>

        <!-- Email Address -->
        <div class="mt-4">
            <x-input-label for="email" :value="__('Email')" />
            <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autocomplete="username" />
            <x-input-error :messages="$errors->get('email')" class="mt-2" />
        </div>

        <!-- Phone Number -->
        <div class="mt-4">
            <x-input-label for="phone_num" :value="__('Phone Number')" />
            <x-text-input id="phone_num" class="block mt-1 w-full" type="text" name="phone_num" :value="old('phone_num')" required />
            <x-input-error :messages="$errors->get('phone_num')" class="mt-2" />
        </div>

        <!-- Group Number -->
        <div class="mt-4">
            <x-input-label for="group_num" :value="__('Group Number')" />
            <x-text-input id="group_num" class="block mt-1 w-full" type="text" name="group_num" :value="old('group_num')" required />
            <x-input-error :messages="$errors->get('group_num')" class="mt-2" />
        </div>

        <!-- Password -->
        <div class="mt-4">
            <x-input-label for="password" :value="__('Password')" />

            <x-text-input id="password" class="block mt-1 w-full"
                            type="password"
                            name="password"
                            required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>

        <!-- Confirm Password -->
        <div class="mt-4">
            <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

            <x-text-input id="password_confirmation" class="block mt-1 w-full"
                            type="password"
                            name="password_confirmation" required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password_confirmation')" class="mt-2" />
        </div>

        <div class="flex items-center justify-end mt-4">

            <a class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('login') }}">
                {{ __('Already registered?') }}
            </a>

            <x-primary-button class="ml-4">
                {{ __('Register') }}
            </x-primary-button>
        </div>
    </form>
</x-guest-layout>

{{-- @extends('template.access')

@section('button')
<a class="btn btn-secondary btn-sm me-2" href="{{route('login')}}">Back to Login</a>
@endsection

@section('content')
<div class="container mt-5">
  <div class="card">
    <div class="card-header text-center">
      <b>Registration Form</b>
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route('register') }}">
            @csrf

            @if (session('error'))
                <p class="alert alert-danger">{{section('error')}}</p>
            @endif

            <!-- Matric ID -->
            <div class="mb-3">
                <label for="matric_id" class="form-label">Matric ID:</label>
                <input type="text" class="form-control @error('matric_id') is-invalid @enderror" name="matric_id" required value="{{old('matric_id')}}">
                @error('matric_id')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <!-- Name -->
            <div class="mb-3">
                <label for="name" class="form-label">Name:</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" required value="{{old('name')}}">
                @error('name')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <!-- Email Address -->
            <div class="mb-3">
                <label for="email" class="form-label">email:</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" required value="{{old('email')}}">
                @error('email')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <!-- Phone Number -->
            <div class="mb-3">
                <label for="phone_num" class="form-label">Phone Number:</label>
                <input type="text" class="form-control @error('phone_num') is-invalid @enderror" name="phone_num" required value="{{old('phone_num')}}">
                @error('phone_num')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <!-- Group Number -->
            <div class="mb-3">
                <label for="group_num" class="form-label">Group Number:</label>
                <input type="text" class="form-control @error('group_num') is-invalid @enderror" name="group_num" required value="{{old('group_num')}}">
                @error('group_num')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <!-- Password -->
            <div class="mb-3">
                <label for="password" class="form-label">Password:</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required value="{{old('password')}}">
                @error('password')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>

            <!-- Confirmation Password -->
            <div class="mb-3">
                <label for="password_confirmation" class="form-label">Password Confirmation:</label>
                <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required value="{{old('password_confirmation')}}">
                @error('password_confirmation')
                    <div class="invalid-feedback">{{$message}}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Register</button>
            {{-- <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 dark:text-gray-400 hover:text-gray-900 dark:hover:text-gray-100 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 dark:focus:ring-offset-gray-800" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>
    
                <button class="ml-4">
                    {{ __('Register') }}
                </button>
            </div> --}}
      {{-- </form>
    </div>
  </div>
</div>
@endsection --}}