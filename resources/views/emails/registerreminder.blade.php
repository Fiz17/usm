<!DOCTYPE html>
<html>
<head>
  <title>{{ $emailData['subject'] }}</title>
</head>
<body style="font-family: Arial, sans-serif;">

  <div style="background-color: #f5f5f5; padding: 20px;">
    <div style="text-align:center;">
      <a href="{{ route('welcome') }}">
        <img src="{{'data:image/png;base64,'.base64_encode(file_get_contents(public_path('/import/kokusmAssets/logokokusm.png')))}}" class="logo" alt="Pusat Rancangan Kokurikulum Logo" height="80" width="80">
      </a>
      <h1 style="color: #333;">{{ $emailData['subject'] }}</h1>
    </div>
    
    
    <div style="background-color: #fff; border: 1px solid #ddd; padding: 20px; margin-top: 20px; margin-bottom: 20px; border-radius: 5px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);">
      <p style="color: #555; font-size: 16px; margin-bottom: 10px;">{{ $emailData['message'] }}</p>
      <p style="color: #555; font-size: 16px;">Please click the button below to register in the WUS101 System:</p>
      <div style="text-align: center;">
        <a href="{{ route('welcome') }}" style="display: inline-block; background-color: #007bff; color: #fff; text-decoration: none; padding: 12px 24px; border-radius: 5px; font-size: 16px; margin-top: 20px; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);">Register Now</a>
      </div>
      <p style="color: #555; font-size: 16px; margin-bottom: 0;">
      Regards,<br>
      Pusat Rancangan Kokurikulum
      </p>

    </div>

  </div>

</body>
</html>
