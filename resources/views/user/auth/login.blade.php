@extends('template.access')

@section('button')
<a class="btn btn-secondary btn-sm me-2" href="{{route('dashboard')}}">Back</a>
@endsection

@section('content')
<div class="container mt-5">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Login</h5>
        <form>
          <div class="mb-3">
            <label for="matricID" class="form-label">Matric ID</label>
            <input type="text" class="form-control" id="matricID" name="matricID" required>
          </div>
          <div class="mb-3">
            <label for="password" class="form-label">Password</label>
            <input type="password" class="form-control" id="password" name="password" required>
          </div>
          <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="rememberMe" name="rememberMe">
            <label class="form-check-label" for="rememberMe">Remember me</label>
          </div>
          <button type="submit" class="btn btn-primary">Login</button>
        </form>
        <p class="mt-3">Don't have an account? <a href="{{route('user-register')}}">Register here!</a></p>
        <p><a href="{{route('admin-login')}}">Login as admin</a></p>
        <p><a href="#">Forgot password?</a></p>
      </div>
    </div>
  </div>
@endsection