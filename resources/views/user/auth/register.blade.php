@extends('template.access')

@section('button')
<a class="btn btn-secondary btn-sm me-2" href="{{route('user-login')}}">Back to Login</a>
@endsection

@section('content')
<div class="container mt-5">
  <div class="card">
    <div class="card-header text-center">
      <b>Registration Form</b>
    </div>
    <div class="card-body">
      <form>
        <div class="mb-3">
          <label for="name" class="form-label">Name:</label>
          <input type="text" class="form-control" id="name" required>
        </div>
        <div class="mb-3">
          <label for="matricNumber" class="form-label">Matric Number:</label>
          <input type="number" class="form-control" id="matricNumber" required>
        </div>
        <div class="mb-3">
          <label for="studentEmail" class="form-label">Student Email:</label>
          <input type="email" class="form-control" id="studentEmail" required>
        </div>
        <div class="mb-3">
          <label for="phoneNumber" class="form-label">Phone Number:</label>
          <input type="tel" class="form-control" id="phoneNumber" required>
        </div>
        <div class="mb-3">
          <label for="groupNumber" class="form-label">Group Number:</label>
          <input type="text" class="form-control" id="groupNumber" required>
        </div>
        <div class="mb-3">
          <label for="password" class="form-label">Password:</label>
          <input type="password" class="form-control" id="password" required>
        </div>
        <div class="mb-3">
          <label for="confirmPassword" class="form-label">Confirm Password:</label>
          <input type="password" class="form-control" id="confirmPassword" required>
        </div>
        <div class="mb-3 form-check">
          <input type="checkbox" class="form-check-input" id="isValid" required>
          <label class="form-check-label" for="isValid">I confirm that all information is valid.</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
  </div>
</div>
@endsection