@extends('template.userhome')

@section('content')
    <!--Containt Section-->
    <div class="container col-xxl-8 px-4 py-5 ">
        <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
          <div class="col-10 col-sm-8 col-lg-6">
            <img src="import/kokusmAssets/menlogo.png" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="500" height="500" loading="lazy">
          </div>
          <div class="col-lg-6">
            <h1 class="display-5 fw-bold text-body-emphasis lh-1 mb-3">WUS101 - Core Entrepreneurship !</h1>
            <p class="lead text-justify">
                The Core Entrepreneurship course, a 2 credits unit compulsory university course, is designed for providing 
                basic exposure to students on entrepreneurship and business management, with emphasis on the experiential 
                aspect of learning while undergoing the process of undertaking business projects within and outside campus.
            </p>
            <div class="d-grid gap-2 d-md-flex justify-content-md-start">
              <a href="#about" class="btn btn-outline-primary btn-lg px-4 me-md-2"> Learn more</a>
            </div>
          </div>
        </div>
      </div>
      <!--End Section-->

      <!-- partnership -->
      <section class="py-5"  id="partnership">
        <div class="container text-center">
            <div class="text-center mb-4">
                <h2>OUR PARTNERSHIPS</h2>
            </div>
          <div class="row">
            <div class="col-6 col-md-3">
              <img src="import/kokusmAssets/anhlogo.png" alt="Partner Logo" class="img-fluid partner-logo" width="150px" height="150px">
            </div>
            <div class="col-6 col-md-3">
              <img src="import/kokusmAssets/jnjlogo.png" alt="Partner Logo" class="img-fluid partner-logo" width="150px" height="150px">
            </div>
            <div class="col-6 col-md-3">
              <img src="import/kokusmAssets/sugarbomblogo.png" alt="Partner Logo" class="img-fluid partner-logo" width="150px" height="150px">
            </div>
            <div class="col-6 col-md-3">
              <img src="import/kokusmAssets/zebra.png" alt="Partner Logo" class="img-fluid partner-logo" width="150px" height="150px">
            </div>
          </div>
        </div>
      </section>
      <!-- End Partnership -->

      <!-- about -->
    <div class="container mt-5 d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
      <section class="container" id="about">
          <div class="text-center mb-4">
              <h2>ABOUT US</h2>
          </div>
          <div class="container">
              <div class="row about-section">
                  <div class="col-lg-6 mb-4">
                      <div class="card h-100">
                          <div class="card-body d-flex flex-column align-items-center">
                              <h2 class="about-heading">Main Objectives</h2>
                              <ul class="about-list">
                                  <li>Providing early exposure to entrepreneurial knowledge to students.</li>
                                  <li>Developing a mindset that encourages students to consider entrepreneurship.</li>
                                  <li>Recognizing students' potential in entrepreneurial activities and projects.</li>
                                  <li>Inculcating self-reliance and an entrepreneurial culture in students.</li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-6 mb-4">
                      <div class="card h-100">
                          <div class="card-body d-flex flex-column align-items-center">
                              <h2 class="about-heading">Learning Outcome</h2>
                              <ul class="about-list">
                                  <li>Understanding and practicing entrepreneurial culture in daily life.</li>
                                  <li>Acquiring practical experience through fieldwork in entrepreneurship.</li>
                                  <li>Evaluating risk and return interrelationships in entrepreneurial projects.</li>
                                  <li>Writing project reports and acceptable business plans.</li>
                                  <li>Presenting project or business plan reports.</li>
                                  <li>Developing and enhancing soft skills.</li>
                                  <li>Developing business networking with business communities.</li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      </section>
      <!-- End About --> 
  
<!--FAQs-->
<div class="container mt-5 d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top" id="FAQs">
  <div class="row">
      <div class="col-md-8 offset-md-2">
          <!-- FAQ Title and Description -->
          <div class="text-center mb-4">
              <h2>FREQUENTLY ASKED QUESTIONS</h2>
              <p>These questions are based on how the system functions and the procedures users need to follow.</p>
          </div>

          <!-- FAQ Cards -->
          <div class="accordion" id="faqAccordion">

              <!-- Question 1 -->
              <div class="card mb-3">
                  <div class="card-header" id="headingOne">
                      <h5 class="mb-0">
                          <i class="fas fa-question-circle"></i> Who can access the system?
                      </h5>
                  </div>
                  <div class="card-body">
                      Only one student in a group of WUS101 courses can access the system. When one student in a group already accessed the system, the others should not create the account again.
                  </div>
              </div>

              <!-- Question 2 -->
              <div class="card mb-3">
                  <div class="card-header" id="headingTwo">
                      <h5 class="mb-0">
                          <i class="fas fa-question-circle"></i> Does the system accept online transactions?
                      </h5>
                  </div>
                  <div class="card-body">
                      The system <b>DOES NOT ACCEPT</b> online transactions. For now, it only accepts cash for every product purchase or rental.
                  </div>
              </div>

              <!-- Question 3 -->
              <div class="card">
                  <div class="card-header" id="headingThree">
                      <h5 class="mb-0">
                          <i class="fas fa-question-circle"></i> What happens when I do not come at the specific time needed?
                      </h5>
                  </div>
                  <div class="card-body">
                      All of your orders or rentals will be invalid.
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<!--End FAQs-->

@endsection