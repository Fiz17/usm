@extends('template.userhome')

@section('style')
<style>
  .alert {
  background-color: #cce5ff; /* Light blue background color */
  border: 1px solid #007bff; /* Blue border */
  color: #007bff; /* Blue text color */
  padding: 10px;
  margin-bottom: 15px;
  border-radius: 5px;
}
</style>
@endsection

@section('content')
<div class="container mt-5">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">
          <h2 class="mb-4">Bicycle Rental Form</h2>
          <hr>
          <div class="mb-4">
            <h6><b>Before you proceed to submit your rental form, please take note of the following rules and guidelines:</b></h6>
                <ul class="list-unstyled">
                    <li>1. Pusat Rancangan Kokurikulum will only accept cash payment.</li>
                    <li>2. Fill in the correct information. Changes may not be possible after submission.</li>
                </ul>
              <h6>Bicycle Rental Pricing Rates:</h6>
                <ul class="list-unstyled">
                  <li><span class="fw-bold">RM10</span> / day</li>
                  <li><span class="fw-bold">RM50</span> / week</li>
                  <li><span class="fw-bold">RM120</span> / month</li>
                </ul>
          </div>
          <hr>
          <!--*****ALERT FEEDBACK*****-->
          @if (session('success'))
            <div class="alert alert-success">{{session('success')}}</div>
          @endif
          <form action="{{route('rent.store')}}" method="POST">
            @csrf
            <!-- Rental ID (Generated Randomly) -->
            <div class="mb-3">
              <label for="rental_id" class="form-label">Rental ID:</label>
              <input type="text" class="form-control" id="rental_id" name="rental_id" value="{{$rental_id}}" readonly>
            </div>
            <!-- Name's Rental -->
            <div class="mb-3">
              <label for="rent_name" class="form-label">Rental's name:</label>
              <input type="text" class="form-control" id="rent_name" name="rent_name" required>
            </div>
            <!-- Matric ID's Rental -->
            <div class="mb-3">
              <label for="rent_matric_id" class="form-label">Rental's matric ID:</label>
              <input type="text" class="form-control" id="rent_matric_id" name="rent_matric_id" required>
            </div>
            <!-- Phone Number Rental -->
            <div class="mb-3">
              <label for="rent_phone_num" class="form-label">Rental's phone number:</label>
              <input type="tel" class="form-control" name="rent_phone_num" id="rent_phone_num" required>
            </div>
            <!-- Bicycle available (Dropdown) -->
            <div class="mb-3">
              <label for="bicycle_id" class="form-label">Available Bicycle (choose any):</label>
              <select class="form-select" name="bicycle_id" id="bicycle_id" required>
                <option value="" selected disabled>Select Bicycle</option>
                @foreach($availableBicycles as $bicycle)
                  <option value="{{ $bicycle->bicycle_id }}">{{ $bicycle->bicycle_id }}</option>
                @endforeach 
                @foreach($finishBicycles as $bicycle)
                  <option value="{{ $bicycle->bicycle_id }}">{{ $bicycle->bicycle_id }}</option>
                @endforeach 
              </select>
            </div>
            <!-- Start Date & End Date -->
            <div class="row mb-3">
              <div class="col">
                <label for="rent_start_date" class="form-label">Start Date:</label>
                <input type="date" class="form-control" id="rent_start_date" name="rent_start_date" required>
              </div>
              <div class="col">
                <label for="rent_end_date" class="form-label">End Date:</label>
                <input type="date" class="form-control" id="rent_end_date" name="rent_end_date" required>
              </div>
            </div>
            <!-- Reminder Box -->
            <div class="alert alert-info">
              Please be aware of PRK operating hours before choosing your date.<br>
              Mon - Fri: 8:10am - 5:10pm<br>
              Lunch Break: Mon - Thu: 1:00pm - 2:00pm, Fri: 12:15pm - 2:45pm
            </div>
            <!-- Total Price -->
            <div class="mb-4">
              <label for="total_payment" class="form-label">Total Payment:</label>
              <input type="text" class="form-control" id="total_payment" name="total_payment" readonly>
            </div>

            <!-- Information about office hours -->
            <div class="mb-1">
              <p>The items can be taken during office hours only.<br>
                  <b>Monday - Friday: 8:10AM - 5:00 PM</b>
              </p>
            </div>

            <!-- Agreement -->
            <div class="form-check mb-3 mt-4">
              <input type="checkbox" class="form-check-input" id="agreement" name="agreement" required>
              <label class="form-check-label" for="agreement">By checking the box and submitting this order, you confirm that you agree with these terms and conditions.</label>
            </div>
            <!-- Submit Button -->
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
  document.addEventListener('DOMContentLoaded', function() {
    const startDateInput = document.querySelector('#rent_start_date');
    const endDateInput = document.querySelector('#rent_end_date');
    const totalPaymentInput = document.querySelector('#total_payment');

    function calculateTotalPayment() {
      const startDate = new Date(startDateInput.value);
      const endDate = new Date(endDateInput.value);

      // Make sure end date is at least one day ahead of start date
      if (endDate <= startDate) {
        endDate.setDate(startDate.getDate() + 1);
        endDateInput.value = endDate.toISOString().substr(0, 10); // Format as 'yyyy-mm-dd'
      }

      const timeDiff = endDate - startDate;
      const days = Math.ceil(timeDiff / (1000 * 60 * 60 * 24));

      const ratePerDay = 10;
      const ratePerWeek = 50;
      const ratePerMonth = 120;
      const rateAfterWeek = 7;
      const rateAfterMonth = 4;

      let totalPayment = 0;

      if (days <= 6) {
        totalPayment = ratePerDay * days;
      } else if (days <= 29) {
        totalPayment = ratePerWeek + (days - 7) * rateAfterWeek;
      } else if (days <= 30) {
        totalPayment = ratePerMonth;
      } else {
        const months = Math.floor(days / 30);
        const remainingDays = days % 30;
        totalPayment = (ratePerMonth * months) + (remainingDays * rateAfterMonth);
      }

      totalPaymentInput.value = totalPayment.toFixed(2); // Display total with two decimal places
    }

    startDateInput.addEventListener('input', calculateTotalPayment);
    endDateInput.addEventListener('input', calculateTotalPayment);
  });
</script>
@endpush