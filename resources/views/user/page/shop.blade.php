@extends('template.userhome')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <h2 class="mb-4">Product Order Form</h2>
                    <hr>
                    <div class="mb-4">
                        <h6><b>Before you proceed to submit your order, please take note of the following rules and guidelines:</b></h6>
                        <ul class="list-unstyled">
                            <li>1. Pusat Rancangan Kokurikulum will only accept cash payment.</li>
                            <li>2. Select the correct item based on your group number. Changes may not be possible after submission.</li>
                            <li>3. Select a suitable date for picking up your order. </li>
                            <li>4. Upon pickup, inspect your order for accuracy and quality.</li>
                        </ul>
                    </div>
                    <hr>
                    <!--*****ALERT FEEDBACK*****-->
                    @if (session('success'))
                        <div class="alert alert-success">{{session('success')}}</div>
                    @endif
                    <form action="{{route('shop.store')}}" method="POST">
                        @csrf
                        <!-- Order ID (Generated Randomly) -->
                        <div class="mb-3">
                            <label for="order_id" class="form-label">Order ID:</label>
                            <input type="text" class="form-control" id="order_id" name="order_id" value="{{$order_id}}" readonly>
                        </div>

                        <!-- Product Name -->
                        <div class="mb-3">
                            <label for="product_id" class="form-label">Product Name:</label>
                            <select class="form-select" name="product_id" id="product_id" required>
                                <option value="" selected disabled>Select a product</option>
                                @foreach($products as $product)
                                <option value="{{ $product->product_id }}" data-price="{{ $product->product_price }}">{{ $product->product_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!-- Quantity and Unit Price -->
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="order_quantity" class="form-label">Quantity:</label>
                                <input type="number" class="form-control" id="order_quantity" name="order_quantity" placeholder="maximum quantity is 2" min="1" max="2" required>
                            </div>
                            <div class="col-md-6">
                                <label for="product_price" class="form-label">Unit Price (RM):</label>
                                <input type="number" class="form-control" id="product_price" name="product_price" readonly>
                            </div>
                        </div>

                        <!-- Total Payment (Calculated based on product price and quantity) -->
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="total_order_payment" class="form-label">Total Payment (RM):</label>
                                <input type="text" class="form-control" id="total_order_payment" name="total_order_payment" readonly>
                            </div>
                            <div class="col-md-6">
                                <label for="pickup_date" class="form-label">Pickup Date:</label>
                                <input type="date" class="form-control" id="pickup_date" name="pickup_date" required>
                            </div>
                        </div>
                        <!-- Reminder Box -->
                        <div class="alert alert-info">
                            Please be aware of PRK operating hours before choosing your date.<br>
                            Mon - Fri: 8:10am - 5:10pm<br>
                            Lunch Break: Mon - Thu: 1:00pm - 2:00pm, Fri: 12:15pm - 2:45pm
                        </div>

                        <!-- Information about office hours -->
                        <div class="mb-1">
                            <p>The items can be taken during office hours only.<br>
                                <b>Monday - Friday: 8:10AM - 5:00 PM</b>
                            </p>
                        </div>
  
                        <!-- Agreement -->
                        <div class="form-check mb-3 mt-4">
                            <input type="checkbox" class="form-check-input" id="agreement" name="agreement" required>
                            <label class="form-check-label" for="agreement">By checking the box and submitting this order, you confirm that you agree with the terms and conditions.</label>
                        </div>

                        <!-- Submit Button -->
                        <button type="submit" class="btn btn-primary">Submit Order</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var productDropdown = document.getElementById('product_id');
        var productPriceField = document.getElementById('product_price');
        var orderQuantityField = document.getElementById('order_quantity');
        var totalPaymentField = document.getElementById('total_order_payment');

        // Update unit price and total payment when product is selected
        productDropdown.addEventListener('change', function () {
            var selectedOption = this.options[this.selectedIndex];
            var unitPrice = selectedOption.getAttribute('data-price');
            if (unitPrice) {
                productPriceField.value = unitPrice;
                updateTotalPayment();
            } else {
                productPriceField.value = '0.00'; // Set a default value when no product is selected
                totalPaymentField.value = '0.00';
            }
        });

        // Update total payment based on quantity and unit price
        orderQuantityField.addEventListener('input', updateTotalPayment);

        function updateTotalPayment() {
            var unitPrice = parseFloat(productPriceField.value) || 0;
            var quantity = parseFloat(orderQuantityField.value) || 0;
            var totalPayment = unitPrice * quantity;
            totalPaymentField.value = totalPayment.toFixed(2);
        }

        // Call updateTotalPayment() initially
        updateTotalPayment();
    });
</script>
@endpush
