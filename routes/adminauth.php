<?php

use App\Http\Middleware\MasterAccess;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\OrderController;
use App\Http\Controllers\admin\RentalController;
use App\Http\Controllers\admin\BicycleController;
use App\Http\Controllers\admin\EarningController;
use App\Http\Controllers\admin\ProductController;
use App\Http\Controllers\admin\UserAccountController;
use App\Http\Controllers\admin\AdminAccountController;
use App\Http\Controllers\admin\StockController;
use App\Http\Controllers\AdminAuth\PasswordController;
use App\Http\Controllers\AdminAuth\NewPasswordController;
use App\Http\Controllers\AdminAuth\VerifyEmailController;
use App\Http\Controllers\AdminAuth\RegisteredUserController;
use App\Http\Controllers\AdminAuth\PasswordResetLinkController;
use App\Http\Controllers\AdminAuth\ConfirmablePasswordController;
use App\Http\Controllers\AdminAuth\AuthenticatedSessionController;
use App\Http\Controllers\AdminAuth\EmailVerificationPromptController;
use App\Http\Controllers\AdminAuth\EmailVerificationNotificationController;

use App\Http\Controllers\ExcelController;
use App\Http\Controllers\ReminderController;

// Guest Authentication Page (Guest)
Route::middleware('guest:admin')->group(function () {

    // Login Page ******************
    Route::get('admin/login', [AuthenticatedSessionController::class, 'create'])->name('admin.login');
    Route::post('admin/login', [AuthenticatedSessionController::class, 'store']);
    //******************************

    // Forgot Password Page ********
    Route::get('/admin/forgot-password', [PasswordResetLinkController::class, 'create'])->name('admin.password.request');
    Route::post('/admin/forgot-password', [PasswordResetLinkController::class, 'store'])->name('admin.password.email');
    //******************************

    // Reset Password Page ********
    Route::get('/admin/reset-password/{token}', [NewPasswordController::class, 'create'])->name('admin.password.reset');
    Route::post('/admin/reset-password', [NewPasswordController::class, 'store'])->name('admin.password.store');
    //******************************            
});

// Admin Authentication Page (Admin)
Route::middleware('auth:admin')->group(function () {

    // Admin register page*********
    Route::get('admin/register', [RegisteredUserController::class, 'create'])
                ->name('admin.register');

    Route::post('admin/register', [RegisteredUserController::class, 'store']);
    //*******************************
    // Admin Verify Email Address (will not use) Page ******************
    Route::get('admin/verify-email', EmailVerificationPromptController::class)->name('admin.verification.notice');
    Route::get('admin/verify-email/{id}/{hash}', VerifyEmailController::class)->middleware(['signed', 'throttle:6,1'])->name('admin.verification.verify');
    Route::post('admin/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])->middleware('throttle:6,1')->name('admin.verification.send');
    // WILL NOT USE LATER

    Route::get('admin/confirm-password', [ConfirmablePasswordController::class, 'show'])->name('admin.password.confirm');
    Route::post('admin/confirm-password', [ConfirmablePasswordController::class, 'store']);
    Route::put('admin/password', [PasswordController::class, 'update'])->name('admin.password.update');
    // Logout Page ******************
    Route::post('admin/logout', [AuthenticatedSessionController::class, 'destroy'])->name('admin.logout');

    Route::middleware(['auth:admin', MasterAccess::class])->group(function () {
        // Admin register page*********
        Route::get('admin/register', [RegisteredUserController::class, 'create'])
        ->name('admin.register');

        Route::post('admin/register', [RegisteredUserController::class, 'store']);

        /* Admin Account Controller */
        Route::resource('admin/adminaccount',AdminAccountController::class); //ada error apa tah
    });

    /* User Account Controller (Monitor) */
    Route::resource('admin/useraccount',UserAccountController::class);

        //User Search Bar
        Route::get('/search-user', [UserAccountController::class, 'search'])->name('user.search');


    /* Earning Controller (Monitor) */
    Route::resource('admin/earning',EarningController::class); //Belum

    /* Order Controller */
    Route::resource('admin/order',OrderController::class);

        //Print Order Receipt
        Route::get('/print-order/{order}', [OrderController::class, 'print'])->name('orders.print');

        //Order Search Bar
        Route::get('/search-order', [OrderController::class, 'search'])->name('order.search');



    /* Rental Controller */
    Route::resource('admin/rental',RentalController::class);

        //Print Rental Receipt
         Route::get('/print-rental/{rental}', [RentalController::class, 'print'])->name('rentals.print');

         //Rental Search Bar
         Route::get('/search-rental', [RentalController::class, 'search'])->name('rental.search');


    /* Product Controller */
    Route::resource('admin/product',ProductController::class); // validation & table track untuk track stock baru

        //Product Search Bar
        Route::get('/search-product', [ProductController::class, 'search'])->name('product.search');


    /* Bicycle Controller */
    Route::resource('admin/bicycle',BicycleController::class); // validation & bicycle_id

        //Bicycle Search Bar
        Route::get('/search-bicycle', [BicycleController::class, 'search'])->name('bicycle.search');


     /* Stock Controller */
     Route::resource('admin/stock',StockController::class);
    

    Route::get('upload', function () {
        return view('admin.create.uploadaccount');
    });

    /* Import & Export Controller */
    Route::group(['prefix' => 'users'], function () {
        Route::post('/usersacc-import', [ExcelController::class, 'import'])->name('users.import');
        Route::get('/report-download', [ExcelController::class, 'export'])->name('weeklyreport.export');
    });
    
    /* Reminder Controller */
    Route::get('/reminder-register', [ReminderController::class, 'registerReminder'])->name('register.reminder');
    Route::get('/reminder-pendingorder', [ReminderController::class, 'pendingorderReminder'])->name('pendingorder.reminder');
    Route::get('/reminder-returnbicycle', [ReminderController::class, 'returnbicycleReminder'])->name('returnbicycle.reminder');
 
});